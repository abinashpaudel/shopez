package aisle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.threecrazyminds.abinash.shopEZ.CommonUtils;
import com.threecrazyminds.abinash.shopEZ.R;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import database.FireBaseItemInfo;
import items.ItemsArrayAdapter;
import user.User;

/**
 * Created by abinash on 3/21/16.
 */
public class AisleListAdapter extends BaseAdapter {

    ArrayList<FireBaseItemInfo> itemInfos;
    Activity context;
    String googlePlaceID;
    String itemName;

    private TextView addAisleButton;
    private ItemsArrayAdapter itemsArrayAdapter;
    private ArrayList<Enum> thumbsState;

    private final String HAS_AISLE = "This Item Has Aisle  Number";
    private final String HAS_SECTION = "This Item Has Section/Sub Sections";

    private final User user;

    public AisleListAdapter(Activity context,
                            ArrayList<FireBaseItemInfo> itemInfos, //name,comment,likes/dislikes
                            String googlePlaceID,
                            String itemName,
                            ItemsArrayAdapter itemsArrayAdapter,
                            TextView addAisleButton) {
        this.itemInfos = itemInfos;
        this.context = context;
        this.googlePlaceID = googlePlaceID;
        this.itemName = itemName;
        this.itemsArrayAdapter = itemsArrayAdapter;
        this.addAisleButton = addAisleButton;

        if (itemInfos != null) {
            thumbsState = new ArrayList<Enum>(Collections.nCopies(itemInfos.size(), CommonUtils.Thumb.NEUTRAL));
        } else {
            thumbsState = new ArrayList<Enum>();
        }

        user = CommonUtils.getUser(context);
    }

    @Override
    public int getCount() {
        return itemInfos.size();
    }

    @Override
    public Object getItem(int position) {
        return itemInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public class ViewHolder {
        TextView aisleNumberTV;
        TextView sectionTV;
        TextView subSectionTV;

        TextView aisleCommentTV;
        ImageView updateAisleInfoBut;
        ImageView aisleThumbsUpBut;
        TextView aisleLikePercentageTV;
        TextView dateTV;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View rowView = convertView;
        LayoutInflater layoutInflater = context.getLayoutInflater();
        if (rowView == null) {
            holder = new ViewHolder();
            rowView = layoutInflater.inflate(R.layout.aisle_row_list, null);

            holder.aisleNumberTV = (TextView) rowView.findViewById(R.id.aisleNumberTV);
            holder.sectionTV = (TextView) rowView.findViewById(R.id.itemSection);
            holder.subSectionTV = (TextView) rowView.findViewById(R.id.itemSubSection);

            holder.aisleCommentTV = (TextView) rowView.findViewById(R.id.aisleCommentTV);
            holder.updateAisleInfoBut = (ImageView) rowView.findViewById(R.id.updateAisleInfoBut);
            holder.aisleThumbsUpBut = (ImageView) rowView.findViewById(R.id.aisleThumbsUpBut);
            holder.aisleLikePercentageTV = (TextView) rowView.findViewById(R.id.aisleLikePercentageTV);
            holder.dateTV = (TextView) rowView.findViewById(R.id.dateTV);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        if(itemInfos.get(position).getAisle().length() > 0) {
            holder.aisleNumberTV.setVisibility(View.VISIBLE);
            holder.sectionTV.setVisibility(View.GONE);
            holder.subSectionTV.setVisibility(View.GONE);
            holder.aisleNumberTV.setText(itemInfos.get(position).getAisle());
        } else {
            holder.aisleNumberTV.setVisibility(View.GONE);
            holder.sectionTV.setVisibility(View.VISIBLE);
            holder.subSectionTV.setVisibility(View.VISIBLE);
            holder.sectionTV.setText(itemInfos.get(position).getSection());
            holder.subSectionTV.setText(itemInfos.get(position).getSubSection());
        }

        holder.dateTV.setText(getShortDate(itemInfos.get(position).getTimeStamp()));
        holder.aisleCommentTV.setText(itemInfos.get(position).getAisleComment());
        holder.aisleLikePercentageTV.setText(itemInfos.get(position).returnLikePercentage() + "%");
        holder.updateAisleInfoBut.setVisibility(View.INVISIBLE); //At first it is invisible

        if (itemInfos.size() >= 3) {
            addAisleButton.setVisibility(View.GONE);
        } else {
            addAisleButton.setVisibility(View.VISIBLE);
        }

        setThumbState(holder, CommonUtils.Thumb.UNKNOWN, thumbsState.get(position));

        if(user.getUserID().length() > 4 && itemInfos.get(position).getUserID().equals(user.getUserID())){
            holder.aisleThumbsUpBut.setVisibility(View.INVISIBLE);
            holder.aisleLikePercentageTV.setVisibility(View.INVISIBLE);
        } else{
            holder.aisleThumbsUpBut.setVisibility(View.VISIBLE);
            holder.aisleLikePercentageTV.setVisibility(View.VISIBLE);
        }

        holder.updateAisleInfoBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupUserAisleInput(position, "");
            }
        });



        holder.aisleThumbsUpBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enum previousState = thumbsState.get(position);

                if (previousState == CommonUtils.Thumb.DOWN || previousState == CommonUtils.Thumb.NEUTRAL) {

                    thumbsState.set(position, CommonUtils.Thumb.UP);

                } else {
                    thumbsState.set(position, CommonUtils.Thumb.DOWN);
                }

                setThumbState(holder, previousState, thumbsState.get(position));



                if (thumbsState.get(position) == CommonUtils.Thumb.DOWN) {
                    holder.updateAisleInfoBut.setVisibility(View.VISIBLE);

                    if(holder.aisleNumberTV.getVisibility() != View.GONE) {
                        holder.aisleNumberTV.setVisibility(View.INVISIBLE);
                    }

                    if(holder.sectionTV.getVisibility() != View.GONE) {
                        holder.sectionTV.setVisibility(View.INVISIBLE);
                        holder.subSectionTV.setVisibility(View.INVISIBLE);
                    }

                    holder.aisleCommentTV.setVisibility(View.INVISIBLE);
                    holder.dateTV.setVisibility(View.INVISIBLE);


                } else {
                    holder.updateAisleInfoBut.setVisibility(View.INVISIBLE);

                    if(holder.aisleNumberTV.getVisibility() !=View.GONE) {
                        holder.aisleNumberTV.setVisibility(View.VISIBLE);
                    }
                    if(holder.sectionTV.getVisibility() !=View.GONE){
                        holder.sectionTV.setVisibility(View.VISIBLE);
                        holder.subSectionTV.setVisibility(View.VISIBLE);
                    }


                    holder.aisleCommentTV.setVisibility(View.VISIBLE);
                    holder.dateTV.setVisibility(View.VISIBLE);
                }
            }
        });


        setThumbState(holder, CommonUtils.Thumb.UNKNOWN, thumbsState.get(position));

        return rowView;

    }

    private void setThumbState(ViewHolder holder, Enum previousState, Enum nowState) {

        if (previousState == CommonUtils.Thumb.NEUTRAL && nowState == CommonUtils.Thumb.UP) {
            rotateThumbs(holder, -360);
            holder.aisleThumbsUpBut.setColorFilter(Color.rgb(0, 153, 0));
        } else if (previousState == CommonUtils.Thumb.UP && nowState == CommonUtils.Thumb.DOWN) {
            rotateThumbs(holder, -180);
            holder.aisleThumbsUpBut.setColorFilter(Color.RED);

        } else if (previousState == CommonUtils.Thumb.DOWN && nowState == CommonUtils.Thumb.UP) {
            rotateThumbs(holder, -360);
            holder.aisleThumbsUpBut.setColorFilter(Color.rgb(0, 153, 0));
        }
        //initial state
        else if (nowState == CommonUtils.Thumb.NEUTRAL) {
            holder.aisleThumbsUpBut.setColorFilter(Color.WHITE);
            holder.aisleThumbsUpBut.setRotation(0);
        } else if (nowState == CommonUtils.Thumb.UP) {
            holder.aisleThumbsUpBut.setColorFilter(Color.rgb(0, 153, 0));
            rotateThumbs(holder, -360);
        } else if (nowState == CommonUtils.Thumb.DOWN) {
            holder.aisleThumbsUpBut.setColorFilter(Color.RED);
            rotateThumbs(holder, -180);
        }
    }

    private void rotateThumbs(ViewHolder vh, float angle) {
        vh.aisleThumbsUpBut.setRotation(0);
        vh.aisleThumbsUpBut.animate().rotation(angle).start();
    }

    public ArrayList<Enum> getThumbsState() {
        return thumbsState;
    }


    public void popupUserAisleInput(final int position,
                                    String uComment) {

        // Implement the pop up window and ask for user's inputAisle about the aisle info
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        final View alertView = context.getLayoutInflater().inflate(R.layout.add_aisle_pop_up, null);

        final EditText inputAisle = (EditText) alertView.findViewById(R.id.editAisle);
        final EditText inputSection = (EditText) alertView.findViewById(R.id.editSection);
        final EditText inputSubSection = (EditText) alertView.findViewById(R.id.editSubSection);



        alertBuilder.setView(alertView); // Set an EditText view to get user inputAisle
        //Assign title as item name
        TextView itemView = (TextView) alertView.findViewById(R.id.item_name);
        itemView.setText(itemName);

        final NumberPicker aisleOrSectionPicker = (NumberPicker) alertView.findViewById(R.id.numberPicker);
        final Spinner aisleOrSectionPickerSpinner = (Spinner) alertView.findViewById(R.id.spinner);




        final String[] aisleOrSectionsArray = new String[]{HAS_AISLE, HAS_SECTION};
        aisleOrSectionPicker.setMinValue(0);
        aisleOrSectionPicker.setDisplayedValues(aisleOrSectionsArray);
        aisleOrSectionPicker.setMaxValue(aisleOrSectionsArray.length - 1);

        //Number Picker change numbers to string
        aisleOrSectionPicker.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return aisleOrSectionsArray[value];
            }
        });

        //spinner adapter
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(context,
                R.layout.spinner_item_info,
                aisleOrSectionsArray);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aisleOrSectionPickerSpinner.setAdapter(spinnerAdapter);

        //spinner onClickListener
        aisleOrSectionPickerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (aisleOrSectionsArray[position].equals(HAS_AISLE)) {
                    inputAisle.setVisibility(View.VISIBLE);
                    inputSection.setVisibility(View.GONE);
                    inputSubSection.setVisibility(View.GONE);

                    //get the keyboard as soon as the alert edittext pop-opens
                    inputAisle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                            }
                        }
                    });

                } else {
                    inputAisle.setVisibility(View.GONE);
                    inputSection.setVisibility(View.VISIBLE);
                    inputSubSection.setVisibility(View.VISIBLE);

                    //get the keyboard as soon as the alert edittext pop-opens
                    inputSection.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                            }
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //number picker listener
        aisleOrSectionPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (aisleOrSectionsArray[newVal].equals(HAS_AISLE)) {
                    inputAisle.setVisibility(View.VISIBLE);
                    inputSection.setVisibility(View.GONE);
                    inputSubSection.setVisibility(View.GONE);

                    //get the keyboard as soon as the alert edittext pop-opens
                    inputAisle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                            }
                        }
                    });

                } else {
                    inputAisle.setVisibility(View.GONE);
                    inputSection.setVisibility(View.VISIBLE);
                    inputSubSection.setVisibility(View.VISIBLE);

                    //get the keyboard as soon as the alert edittext pop-opens
                    inputSection.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                            }
                        }
                    });
                }
            }
        });



        getAisleOrSection(alertBuilder,alertView,imm,position,uComment);





    }

    private void getAisleOrSection(final AlertDialog.Builder alertBuilder,
                                   final View alertView,
                                   final InputMethodManager imm,
                                   final int position,
                                   final String uComment) {


        final EditText inputAisle = (EditText) alertView.findViewById(R.id.editAisle);
        final EditText inputSection = (EditText) alertView.findViewById(R.id.editSection);
        final EditText inputSubSection = (EditText) alertView.findViewById(R.id.editSubSection);

        final TextView commentView = (TextView) alertView.findViewById(R.id.editComment);


        inputAisle.setSelection(inputAisle.getText().toString().length()); //move the cursor to the end

        //Save the entered info when save button is clicked
        alertBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                String aisleNumber = inputAisle.getText().toString();
                String aisleSection = inputSection.getText().toString();
                String aisleSubSection = inputSubSection.getText().toString();

                String comment = commentView.getText().toString();

                if (aisleNumber.trim().length() <= 0 && inputAisle.getVisibility() == View.VISIBLE) {
                    CommonUtils.shakeAndVibrate(context, inputAisle);
                    Toast.makeText(context, "Aisle can't be empty", Toast.LENGTH_SHORT).show();
                    popupUserAisleInput(position, comment);
                    return;
                } else if(aisleSection.trim().length()<=0 && inputSection.getVisibility() == View.VISIBLE ){
                    CommonUtils.shakeAndVibrate(context, inputAisle);
                    Toast.makeText(context, "Section can't be empty", Toast.LENGTH_SHORT).show();
                    popupUserAisleInput(position, comment);
                    return;
                } else if(inputAisle.getVisibility() == View.VISIBLE){
                    aisleSection = "";
                    aisleSubSection="";
                } else if(inputSection.getVisibility() == View.VISIBLE){
                    aisleNumber = "";
                }


                if (googlePlaceID.length() > 4) {
                    itemsArrayAdapter.saveToLocalFireBase(itemName, aisleNumber,comment, aisleSection, aisleSubSection);
                    itemsArrayAdapter.saveToFireBase(itemName, true); //Firebase takes from local database, give points to user
                }

                thumbsState.add(CommonUtils.Thumb.UP);


                notifyDataSetChanged();
            }
        });
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        commentView.setText(uComment);
        alertBuilder.show();
        imm.showSoftInput(inputAisle, InputMethodManager.SHOW_FORCED); //force keyboard after show

    }



    private String getShortDate(long millis) {
        SimpleDateFormat sdfSN = new SimpleDateFormat("dd/MM/yyyy");
        String dateAndTime = sdfSN.format(new Date(millis));
        return dateAndTime;
    }


}
