package com.threecrazyminds.abinash.shopEZ;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

/**
 * Created by abinash on 2/19/16.
 */
public class ShoppingListsAdapter extends BaseAdapter{

    Activity context;
    ArrayList<ShoppingList> shopList;
    GoogleApiClient mGoogleApiClient;
    ShowcaseView sv;

    public ShoppingListsAdapter(Activity context,
                                ArrayList<ShoppingList> shopList,
                                GoogleApiClient mGoogleApiClient,
                                ShowcaseView sv){
        this.context = context;
        this.shopList = shopList;
        this.mGoogleApiClient = mGoogleApiClient;
        this.sv = sv;
    }
    @Override
    public int getCount() {
        return shopList.size();
    }
    @Override
    public Object getItem(int position) {
        return shopList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return shopList.get(position).getId();
    }

    public class ViewHolder{
        ImageView allDoneIV;
        TextView shoppingTitleTV;
        ImageView overFlowButIV;
        LinearLayout findAisleBut;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final int myPosition = position;
        final ViewHolder holder;
        View rowView = convertView;
        LayoutInflater layoutInflater = context.getLayoutInflater();
        if(rowView == null) {
            holder = new ViewHolder();
            rowView = layoutInflater.inflate(R.layout.shopping_card_view, null);
            holder.shoppingTitleTV = (TextView) rowView.findViewById(R.id.rowtext_shoplist);
            holder.allDoneIV = (ImageView) rowView.findViewById(R.id.allDone_shoplist);
            holder.overFlowButIV = (ImageView) rowView.findViewById(R.id.overFlowButton_shoplist);
            holder.findAisleBut = (LinearLayout) rowView.findViewById(R.id.findAisleBut);
            rowView.setTag(holder);
        } else{
            holder = (ViewHolder) rowView.getTag();
        }

        holder.shoppingTitleTV.setText(shopList.get(position).getName());
        if(shopList.get(position).getDone()){
            holder.allDoneIV.setVisibility(View.VISIBLE);
            holder.shoppingTitleTV.setTextColor(Color.rgb(189,189,189));
        }
        else{
            holder.allDoneIV.setVisibility(View.GONE);
            holder.shoppingTitleTV.setTextColor(Color.BLACK);
        }

        holder.findAisleBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(sv!=null){
                    sv.hide();
                }

                if(CommonUtils.isNetworkAvailable(context) == false){
                    CommonUtils.showSimpleAlertDialog("NO CONNECTION", "Sorry, I won't be able to determine grocery" +
                            "store without internet connection",context);
                    return;
                }



                final ProgressDialog pd = new ProgressDialog(context);
                pd.setIndeterminate(true);
                pd.setMessage("Getting Location..");
                pd.show();
                CommonUtils.startPlacePicker(mGoogleApiClient, context, position);

                Runnable r1 = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            pd.dismiss();
                        } catch(Exception e){
                            System.out.println("Dialog dismissed after activity finished....");
                        }
                    }
                };
                new android.os.Handler().postDelayed(r1, 1500);
                //startPlacePicker(myPosition);
            }
        });

        Animation shopBounce = AnimationUtils.loadAnimation(context,R.anim.zoom_in);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(shopBounce);
        animationSet.setStartOffset((long)(position*500));
        holder.findAisleBut.startAnimation(animationSet);

        holder.overFlowButIV.setOnClickListener(new ShoppingOverFlowOnClickListener(context,
                position,
                shopList,
                rowView,
                mGoogleApiClient
                ));

        if(position == 0) {
            showAisleTutorial(holder.findAisleBut);
        }


        return rowView;
    }

    private void showAisleTutorial(final LinearLayout findAisleBut){
        if(CommonUtils.activityStartedFirst(CommonUtils.AISLE_LIST_ACTIVITY,context) == true &&
                shopList.size() == 1){

            Target butTarget = new Target() {
                @Override
                public Point getPoint() {
                    int[] location = new int [2];
                    findAisleBut.getLocationOnScreen(location);

                    int x = Math.round(location[0]);
                    int y = Math.round(location[1]);
                    return new Point(x, y);
                }
            };

            sv = new ShowcaseView.Builder(context)
                    .setTarget(butTarget)
                    .setContentTitle("Click to find Aisles")
                    .setContentText("Select the store with a proper name from \"Place Finder\". (Powered by Google)")
                    .hideOnTouchOutside()
                    .build();

            sv.hideButton();
            sv.setHideOnTouchOutside(true);
            sv.setClickable(false);


            sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                    CommonUtils.setFirstTimeActivityToFalse(CommonUtils.AISLE_LIST_ACTIVITY,context);
                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

                }
            });
        }

    }

}
