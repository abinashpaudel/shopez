package com.threecrazyminds.abinash.shopEZ;

/**
 * Created by abinash on 3/5/16.
 */
public class ShoppingList {
    private String name;
    private Boolean done;
    private Long id;



    public ShoppingList(String name, Boolean done){
        this.name = name;
        this.done = done;
    }

    public String getName(){
        return name;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
