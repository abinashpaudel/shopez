package com.threecrazyminds.abinash.shopEZ;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.SyncStateContract;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import database.DBInterface;
import items.Item;
import user.User;

/**
 * Created by abinash on 3/12/16.
 */
public class CommonUtils {

    private static final String ITEM_LIST = "itemList";
    private static final String THUMB_LIST = "thumbsState";
    private static final String SHOPPING_LIST_ID = "shoppingListID";

    private static final String USER = "user";

    private static final String SHOP_LIST_NAME="shoppingListName";
    private static final String SHOP_LIST_ID="shoppingListID";
    private static final String STORE_NAME="storeName";
    private static final String STORE_ADDRESS="storeAddress";
    private static final String GOOGLE_PLACE_ID="googlePlaceID";

    private static final String USER_AISLE_INPUT_TRACKER="userInputAisleTracker";

    private static final int PLACE_PICKER_REQUEST = 2;

    public static final String SHOPPING_LIST_ACTIVITY="shoppingListActivity";
    public static final String ITEM_LIST_ACTIVITY="shoppingListActivity";
    public static final String AISLE_LIST_ACTIVITY="aisleListActivity";
    public static final String GOOGLE_MAP_ACTIVITY="googleMapActivity";
    public static final String SHARE_LIST_ACTIVITY = "shareListActivity";
    public static final String ADD_FRIEND_ACTIVITY = "addFriendActivity";




    private static Gson gson;

    static{
        gson = new Gson();
    }

    public static enum Thumb{
        NEUTRAL,UP,DOWN,UNKNOWN
    }


    private static SharedPreferences getSP(Context context){
        return context.getSharedPreferences("com.threecrazyminds.abinash.shopEZ", Context.MODE_PRIVATE);
    }

    public static User getUser(Context context){
        String jsonUser = getSP(context).getString(USER, "");

        User user = null;
        if(jsonUser!=null && !jsonUser.isEmpty()){
            Type itemType = new TypeToken<User>() {
            }.getType();
            user = gson.fromJson(jsonUser, itemType);
        }

        return user;
    }

    public static boolean removeUserInfo(Context context){
        return getSP(context).edit().remove(USER).commit();
    }

    public static boolean storeUser(Context context, User user){
        String jsonUser = gson.toJson(user);
        return getSP(context).edit().putString(USER, jsonUser).commit();
    }

    public static long saveShoppingListAndItemListToDB(String shoppingListName,
                                                       long shoppingListID,
                                                       boolean done,
                                                       ArrayList<Item> itemList,
                                                       Context context
                                                       ) {
        DBInterface db = DBInterface.getInstance(context);
        //save shopping List Name
        shoppingListID = db.addOrUpdateShoppingListName(shoppingListName, shoppingListID, new Date().getTime());

        if (shoppingListID < 0) {
            Toast.makeText(context, "Couldn't save shopping list", Toast.LENGTH_SHORT);
        }

        long errorWhileSavingShoppingListState = db.saveShoppingListState(shoppingListID, done);

        if (errorWhileSavingShoppingListState < 0) {
            Toast.makeText(context, "Couldn't save item lists", Toast.LENGTH_SHORT);
        }

        db.saveItems(shoppingListID, itemList);
//        db.saveThumbState(shoppingListID,itemList,thumbList);

        return shoppingListID;


    }


    public static void saveShoppingListInfo(String myShoppingListName, long myShoppingListID, String storeName, String storeAddress,
                                            String googlePlaceID, Context context){

        getSP(context).edit().putString(SHOP_LIST_NAME,myShoppingListName).commit();
        getSP(context).edit().putLong(SHOP_LIST_ID,myShoppingListID).commit();
        getSP(context).edit().putString(STORE_NAME,storeName).commit();
        getSP(context).edit().putString(STORE_ADDRESS,storeAddress).commit();
        getSP(context).edit().putString(GOOGLE_PLACE_ID,googlePlaceID).commit();

    }

    public static String getShopListName(Context context){
        return getSP(context).getString(SHOP_LIST_NAME, "");
    }
    public static Long getShoppingListID(Context context){
        return getSP(context).getLong(SHOP_LIST_ID, 0);
    }
    public static String getStoreName(Context context){
        return getSP(context).getString(STORE_NAME, "");
    }
    public static String getStoreAddress(Context context){
        return getSP(context).getString(STORE_ADDRESS, "");
    }
    public static String getGooglePlaceId(Context context){
        return getSP(context).getString(GOOGLE_PLACE_ID,"");
    }


    public static void hideKeyboard(Context context, EditText et){
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.showSoftInputFromInputMethod(et.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
    }

    public static void showKeyboard(Context context, EditText et){
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
    }


    public static boolean storeItemList(ArrayList<Item> itemList, Context context){
        String jsonItems = gson.toJson(itemList);
        return getSP(context).edit().putString(ITEM_LIST, jsonItems).commit();
    }

    public static boolean removeItemLists(Context context){
        return getSP(context).edit().remove(ITEM_LIST).commit();
    }

    public static ArrayList<Item> getItemLists(Context context){

        String jsonItems = getSP(context).getString(ITEM_LIST, "");

        ArrayList<Item> myItemList = null;
        if(jsonItems!=null && !jsonItems.isEmpty()){
            Type itemType = new TypeToken<ArrayList<Item>>() {
            }.getType();
            myItemList = gson.fromJson(jsonItems, itemType);
        }

        return myItemList;
    }

    public static boolean storeThumbList(ArrayList<Enum> thumbList, Context context){
        String jsonThumbs = gson.toJson(thumbList);
        return getSP(context).edit().putString(THUMB_LIST, jsonThumbs).commit();
    }

    public static boolean removeThumbList(Context context){
        return getSP(context).edit().remove(THUMB_LIST).commit();
    }

    public static ArrayList<Enum> getThumbList(Context context){
        String jsonThumbs = getSP(context).getString(THUMB_LIST, "");

        ArrayList<Enum> myThumbList = null;
        if(jsonThumbs!=null && !jsonThumbs.isEmpty()){
            Type thumbType = new TypeToken<ArrayList<Thumb>>() {
            }.getType();
            myThumbList = gson.fromJson(jsonThumbs, thumbType);
        }
        return myThumbList;
    }

    public static boolean storeUserInputAisleTracker(ArrayList<Boolean> userInputAisleTracker, Context context){
        String jsonUserInputAisleTracker = gson.toJson(userInputAisleTracker);
        return getSP(context).edit().putString(USER_AISLE_INPUT_TRACKER, jsonUserInputAisleTracker).commit();
    }

    public static boolean removeUserInputAisleTracker(Context context){
        return getSP(context).edit().remove(USER_AISLE_INPUT_TRACKER).commit();
    }

    public static ArrayList<Boolean> getUserInputAisleTracker(Context context){
        String jsonUserInputAisleTracker = getSP(context).getString(USER_AISLE_INPUT_TRACKER,"");

        ArrayList<Boolean> myUserInputAisleTracker = null;
        if(jsonUserInputAisleTracker!=null && !jsonUserInputAisleTracker.isEmpty()){
            Type userInputAisleTrackerType = new TypeToken<ArrayList<Boolean>>(){
            }.getType();
            myUserInputAisleTracker = gson.fromJson(jsonUserInputAisleTracker,userInputAisleTrackerType);
        }
        return myUserInputAisleTracker;
    }

    public static void setSelectionColor(View view, Context context){
        view.requestFocusFromTouch();
        view.setBackgroundColor(ContextCompat.getColor(context, R.color.bright_foreground_disabled_holo_light));
    }

    public static void deselect(View view, Context context){
        view.requestFocusFromTouch();
        view.setBackgroundColor(0);
    }

    public static ArrayList<String> getAssetListFromFile(String fileName,Context context){

        BufferedReader reader = null;
        ArrayList<String> myList = new ArrayList<String>();

        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open(fileName)));

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                myList.add(mLine);
            }
        } catch (IOException e) {
            System.out.println("IO Exception..");
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return myList;
    }

    public static void shakeAndVibrate(Context context, EditText editTextToVibrate) {
        Animation myAnimation = AnimationUtils.loadAnimation(context, R.anim.shake);
        Vibrator vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vib.vibrate(30);
        editTextToVibrate.startAnimation(myAnimation);
        return;
    }

    public static void startPlacePicker(GoogleApiClient mGoogleApiClient, Activity mainActivityContext, int position){
        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            Toast.makeText(mainActivityContext, "Enable Location Services or Check your internet connection.", Toast.LENGTH_SHORT).show();
            return;
        }

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            ShoppingListsMainActivity.shoppingListPositionToPlacePicker = position;
            mainActivityContext.startActivityForResult(builder.build(mainActivityContext), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {

            e.printStackTrace();
            Toast.makeText(mainActivityContext, "Google Play Services Unavailable......", Toast.LENGTH_LONG).show();
        }
    }

    public static int minIndex (ArrayList<Integer> list) {
        return list.indexOf (Collections.min(list)); }

    public static int maxIndex (ArrayList<Integer> list) {
        return list.indexOf (Collections.max(list)); }

    public static int midIndex(ArrayList<Integer> list){
        return list.indexOf (Collections.min(list));
    }

    public static String millisToDate(long millis){

        return DateFormat.getDateInstance(DateFormat.SHORT).format(millis);
        //You can use DateFormat.LONG instead of SHORT

    }

    public  static void showSimpleAlertDialog(String title, String message, Context context){

        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public static void showCustomAlertDialog(String title, String message, Activity activity){

        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
        final AlertDialog alertDialog;
        final View alertView =   activity.getLayoutInflater().inflate(R.layout.custom_alert_dialog, null);

        final TextView titleTV = (TextView) alertView.findViewById(R.id.titleTV);
        final TextView messageTV = (TextView) alertView.findViewById(R.id.messageTV);


        titleTV.setText(title);
        messageTV.setText(message);

        alertBuilder.setView(alertView);

        alertBuilder.setPositiveButton("OK", null);

        alertDialog = alertBuilder.create();

        alertDialog.show();

        ((ViewGroup)alertDialog.getWindow().getDecorView())
                .getChildAt(0).startAnimation(AnimationUtils.loadAnimation(
                activity, R.anim.zoom_in));

        alertDialog.setCanceledOnTouchOutside(true);
    }

    public static int getResourceId(String pVariableName, String pResourcename, Context context) {
        try {
            return context.getResources().getIdentifier(pVariableName, pResourcename, context.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static View getEmptyFooterView(Context context){
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View footerView = layoutInflater.inflate(R.layout.empty_listview_footer, null, false);
        return footerView;
    }

    public static  boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }



    public static boolean activityStartedFirst(String activityName, Context context){
        return getSP(context).getBoolean(activityName, true);
    }

    public static void setFirstTimeActivityToFalse(String activityName, Context context){
        getSP(context).edit().putBoolean(activityName,false).commit();
    }



}
