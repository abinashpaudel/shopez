package com.threecrazyminds.abinash.shopEZ;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ActionViewTarget;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

import database.DBInterface;
import database.FireBaseShoppingList;
import database.FireBaseUtils;
import items.ItemListsActivity;
import login.Login_Activity;
import navigationDrawer.NavDrawerConstants;
import navigationDrawer.NavDrawerListAdapter;
import user.User;
import user.UserUtils;




public class ShoppingListsMainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    public ListView shoppingListNamesView;
    public ListView navDrawerLV;
    private ArrayList<ShoppingList> shopList;

    static final int REQUEST_SHOPPINGLISTNAME = 1;
    static final int PLACE_PICKER_REQUEST = 2;

    ShoppingListsAdapter shoppingListNameAdapter;
    NavDrawerListAdapter navDrawerAdapter;

    private Activity mainActivity = this;

    private GoogleApiClient mGoogleApiClient;
    private String storeName;
    private String storeAddress;
    private String googlePlaceID;
    public static int shoppingListPositionToPlacePicker;

    private ValueEventListener sharedListWatcher;
    private Firebase sharedListRef;

    private User user;


    private ActionBarDrawerToggle mDrawerToggle;

    TextView titleTV;
    ImageView downloadBut;

    DBInterface db;

    private ArrayList<String> navActions;
    private ArrayList<String> navActionsImage;

    Toolbar toolbar;

    FloatingActionButton fab;

    ShowcaseView sv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_main_shopping_lists);
        } else {
            setContentView(R.layout.activity_main_shopping_lists_landscape);
        }
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_main_shopping_lists);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        titleTV = (TextView) findViewById(R.id.toolbar_title);
        titleTV.setText("Home");

        downloadBut = (ImageView) findViewById(R.id.btn_download);
        downloadBut.setVisibility(View.VISIBLE);


        user = CommonUtils.getUser(mainActivity);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayShowHomeEnabled(false); // show or hide the default home button
            ab.setDisplayHomeAsUpEnabled(false);
            ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
            ab.setDisplayShowTitleEnabled(true);
        }


        shoppingListNamesView = (ListView) findViewById(R.id.shoppingLV);


        db = DBInterface.getInstance(mainActivity);

        extractShoppingListsArray(); //extract shoppingListsArray from Database

        setGoogleApiClient();

        shoppingListNamesView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                startItemListActivity(position);
//               startPlacePickerIntent(position);

            }
        });


        shoppingListNameAdapter = new ShoppingListsAdapter(this, shopList,mGoogleApiClient,sv);
        shoppingListNamesView.setAdapter(shoppingListNameAdapter);

        storeName = "";
        storeAddress = "";
        googlePlaceID="";

        setOnLongClickListener();

        shoppingListNamesView.setEmptyView(findViewById(R.id.emptyIV));

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(sv!=null){
                    sv.hide();
                }

                int newPosition = shopList.size();
                startItemListActivity(newPosition);

            }
        });

        downloadBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptDownload();
            }
        });

        setNavDrawer();

        watchForSharedList();

        showTutorial();


    }

    private void showTutorial(){

        if(CommonUtils.activityStartedFirst(CommonUtils.SHOPPING_LIST_ACTIVITY,mainActivity) == true){
            sv = new ShowcaseView.Builder(mainActivity)
                    .setTarget(new ViewTarget(R.id.fab,this))
                    .setContentTitle("Create your first shopping list")
                    .setContentText("Click the + button below")
                    .hideOnTouchOutside()
                    .build();

            sv.hideButton();
            sv.setHideOnTouchOutside(true);
            sv.setClickable(false);


            sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                    CommonUtils.setFirstTimeActivityToFalse(CommonUtils.SHOPPING_LIST_ACTIVITY,mainActivity);
                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView) {

                }

                @Override
                public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

                }
            });
        }

    }

    private void watchForSharedList(){
        String userEmail = user.getUserEmail();

        if(CommonUtils.isNetworkAvailable(mainActivity) == false || userEmail.length()<4){
            return;
        }

        String decodedUserEmail = UserUtils.decodeEmail(userEmail);
        final Animation pulseAnim = AnimationUtils.loadAnimation(mainActivity, R.anim.pulse);

        sharedListRef = new Firebase(FireBaseUtils.FIREBASE_URL+"/sharedWith/"+decodedUserEmail);

        sharedListWatcher = sharedListRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    long totalChildren = dataSnapshot.getChildrenCount();

                    if (totalChildren == 0) {
                        //NO CONTENT", "No shared lists to download...", mainActivity);
                        //hide button
                        downloadBut.clearAnimation();
                        downloadBut.setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.White));


                    } else{
                        pulseAnim.setRepeatCount(10);
                        downloadBut.setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.Tomato));
                        downloadBut.setAnimation(pulseAnim);
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    private void destructListeners(){
        if(sharedListRef!=null && sharedListWatcher!=null){
            sharedListRef.removeEventListener(sharedListWatcher);
            sharedListRef = null;
        }
    }

    private void promptDownload(){
        String userEmail = user.getUserEmail();
        if(userEmail.length()<4){
            CommonUtils.showCustomAlertDialog("NOT SIGNED IN", "You must be signed in to download the list shared to you", mainActivity);
            return;
        }

        if(CommonUtils.isNetworkAvailable(mainActivity) == false){
            CommonUtils.showCustomAlertDialog("NO CONNECTION", "There seems to be a problem with internet connection", mainActivity);
            return;
        }

        final ProgressDialog pd = new ProgressDialog(mainActivity);
        pd.setIndeterminate(true);
        pd.setMessage("Checking for shared lists from friends");
        pd.show();

        String decodedUserEmail = UserUtils.decodeEmail(userEmail);

        Firebase refToSharedWithEmail = new Firebase(FireBaseUtils.FIREBASE_URL+"/sharedWith/"+decodedUserEmail);

        refToSharedWithEmail.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    long totalChildren = dataSnapshot.getChildrenCount();

                    if(totalChildren ==0){
                        try {
                            pd.dismiss();
                        } catch(Exception e){
                            System.out.println("Dialog dismissed after activity finished....");
                        }
                        CommonUtils.showSimpleAlertDialog("NO CONTENT", "No shared lists to download...",mainActivity);
                    }

                    for (DataSnapshot shoppingListPushIDDSS : dataSnapshot.getChildren()) {
                        final String shoppingListPushID = shoppingListPushIDDSS.getKey();
                        final String shoppingListOwner = shoppingListPushIDDSS.child("ownerEmail").getValue(String.class);

                        try {
                            pd.dismiss();
                        } catch(Exception e){
                            System.out.println("Dialog dismissed after activity finished....");
                        }
                        new AlertDialog.Builder(mainActivity)
                                .setTitle("Confirm")
                                .setMessage("Download shopping list shared by "+ shoppingListOwner + "?")
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        extractShoppingListThenDeleteSnapShot(shoppingListPushID,shoppingListOwner,dataSnapshot);
                                    }
                                })
                                .setNegativeButton("Discard", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dataSnapshot.getRef().child(shoppingListPushID).removeValue();
                                    }
                                })
                                .setCancelable(true)

                                .show();
                    }
                } else{
                    try {
                        pd.dismiss();
                    } catch(Exception e){
                        System.out.println("Dialog dismissed after activity finished....");
                    }
                    CommonUtils.showSimpleAlertDialog("NO CONTENT", "No shared lists to download...",mainActivity);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    private void extractShoppingListThenDeleteSnapShot(final String shoppingListPushID, final String shoppingListOwner,
                                                       final DataSnapshot dataSnapshotToDelete){

        final ProgressDialog pd = new ProgressDialog(mainActivity);
        pd.setIndeterminate(true);
        pd.setMessage("Downloading shared list...");
        pd.show();

        final Firebase rootRef = new Firebase(FireBaseUtils.FIREBASE_URL);

        rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //if it doesn't exist, then the list has already been deleted because it's more than 10 days old
                if(dataSnapshot.child("userSharedList").child(shoppingListOwner).child(shoppingListPushID).exists() == false){
                    Toast.makeText(mainActivity,"Cannot download the shopping list. It's no longer available",Toast.LENGTH_SHORT);
                    dataSnapshotToDelete.getRef().child(shoppingListPushID).removeValue();
                } else {
                    //download the data
                    System.out.println(dataSnapshot.child("userSharedList").child(shoppingListOwner).
                            child(shoppingListPushID).getRef().toString());


                    FireBaseShoppingList fireBaseShoppingList = dataSnapshot.child("userSharedList").child(shoppingListOwner).
                            child(shoppingListPushID).getValue(FireBaseShoppingList.class);


                    fireBaseShoppingList.setShoppingListID(0);

                    long myShoppingListID = CommonUtils.saveShoppingListAndItemListToDB(fireBaseShoppingList.getShoppingListName(),
                            fireBaseShoppingList.getShoppingListID(),
                            false,
                            fireBaseShoppingList.getItemLists(),
                            mainActivity);


                    ArrayList<ShoppingList> newShopList = db.getShoppingLists();
                    shopList.clear();
                    for(int i=0; i<newShopList.size(); i++){
                        shopList.add(newShopList.get(i));
                    }

                    shopList = db.getShoppingLists();

                    shoppingListNameAdapter.notifyDataSetChanged();

                    if(myShoppingListID>0){
                        CommonUtils.showCustomAlertDialog("SUCCESS","Sweet!! ShoppingList \""+fireBaseShoppingList.getShoppingListName()+
                        "\" is now added to your list..",mainActivity);
                    }

                    //delete the sharedListInfo
                    dataSnapshotToDelete.getRef().child(shoppingListPushID).removeValue();
                }

                try {
                    pd.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {


            }
        });


    }


    private void setGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage(this, 0, this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public void startPlacePickerIntent(int myPosition){
        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            Toast.makeText(mainActivity, "Enable Location Services or Check your internet connection.", Toast.LENGTH_SHORT).show();
            return;
        }

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            shoppingListPositionToPlacePicker = myPosition; //save this position, onSaveInstanceState method will be called before
            //startActivityForResult method will be called.
            startActivityForResult(builder.build(mainActivity), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {

            e.printStackTrace();
            Toast.makeText(mainActivity, "Google Play Services Unavailable......", Toast.LENGTH_LONG).show();
        }
    }

    private void startItemListsActivity(int position) {

        Intent createShoppingList = new Intent(mainActivity, ItemListsActivity.class);

        Bundle extras = new Bundle();
        extras.putInt("position", position);
        extras.putLong("shoppingListID", shopList.get(position).getId());
        extras.putString("shoppingListName", shopList.get(position).getName());
        createShoppingList.putExtras(extras);

        mainActivity.startActivityForResult(createShoppingList, ShoppingListsMainActivity.REQUEST_SHOPPINGLISTNAME);
        Log.i("Position Clicked", position + "");
    }

    private long getShoppingListID(int positionClicked) {
        if (positionClicked < shopList.size()) {
            return shopList.get(positionClicked).getId();
        }
        return 0;
    }

    private String getShoppingListName(int positionClicked) {
        if (positionClicked < shopList.size()) {
            return shopList.get(positionClicked).getName();
        }
        return "";
    }

    private void startItemListActivity(int position) {

        Intent createShoppingList = new Intent(getApplicationContext(), ItemListsActivity.class);

        String myShoppingListName = getShoppingListName(position);
        long myShoppingListID = getShoppingListID(position);
        CommonUtils.saveShoppingListInfo(myShoppingListName,myShoppingListID,storeName,storeAddress,googlePlaceID,mainActivity);
        startActivity(createShoppingList);
        overridePendingTransition(R.transition.left_in, R.transition.right_out);

        startActivity(createShoppingList);
        overridePendingTransition(R.transition.left_in, R.transition.right_out);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        //getMenuInflater().inflate(R.menu.item_overflow_menu,menu);
//        getMenuInflater().inflate(R.menu.menu_main_shopping_activity, menu);
//        boolean loginVisibleState = true;
//        boolean logoutVisibleState = false;
//        if(user.getUserID().length() >0){
//            loginVisibleState = false;
//            logoutVisibleState = true;
//        }
//
//        menu.findItem(R.id.login).setVisible(loginVisibleState);
//
//        //show logout
//        menu.findItem(R.id.logout).setVisible(logoutVisibleState);
//
//        forceShowIcons(menu);
//
//
        return true;
    }

    private void forceShowIcons(Menu menu){
        try {
            Field[] fields = menu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(menu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Icons couldn't be showed");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }


        return true;
    }

    private void switchToLogin(){
        boolean removed = CommonUtils.removeUserInfo(this);

        if (removed == false) {
            Toast.makeText(mainActivity, "Sorry couldn't get out of here. Try Again...", Toast.LENGTH_LONG);
        }

        Intent intent = new Intent(getApplicationContext(),Login_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.transition.left_in, R.transition.left_out);
        finish();
    }

    private void logout(){
        new AlertDialog.Builder(this)
                .setTitle("Confirm")
                .setMessage("Yes I really want to get out of here!!")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        switchToLogin();
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }


    private void deleteCheckedShoppingListFromLast() {

        if(shopList != null) {
            for (int i = shopList.size()-1; i >= 0; i--) {//delete from last
                if(shopList.get(i).getDone()){
                    shopList.remove(i);
                }
            }
            shoppingListNameAdapter.notifyDataSetChanged();
        }
    }

    public void extractShoppingListsArray() {

        shopList = db.getShoppingLists();

        if(shopList == null) {
            shopList = new ArrayList<ShoppingList>();
        }

    }

    private void removeShoppingListNotifyDBAndAD(int i) {

        shopList.remove(i);
        db.saveShoppingLists(shopList);
        shoppingListNameAdapter.notifyDataSetChanged();

    }


    private void setOnLongClickListener() {
        shoppingListNamesView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int i, long id) {
                final int position = i;
                //Set an alert dialog before deletion
                new AlertDialog.Builder(mainActivity)
                        .setTitle("Confirm Deletion")
                        .setMessage("Delete Shopping List \"" + shopList.get(position).getName() + "\"?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                removeShoppingListNotifyDBAndAD(position);

                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();

                return true;
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //If the result is from Google API for Places
        if (requestCode == PLACE_PICKER_REQUEST) {
            processPlacePickerRequest(requestCode, resultCode, data);
        }
    }

    private void processPlacePickerRequest(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(mainActivity, data);
            storeName = place.getName().toString();
            storeAddress = place.getAddress().toString();

            if(storeAddress.contains(storeName)){
                CommonUtils.showSimpleAlertDialog("Not a proper store","The address you selected doesn't appear to " +
                        "be an adress of a business location. \n\n Try Again! ",mainActivity);
                storeName = "";
                storeAddress = "";
                return;
            }

            googlePlaceID = place.getId().toString();

            startItemListActivity(shoppingListPositionToPlacePicker);


        } else {
            Toast.makeText(mainActivity, "Please select your store for shopping!!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        String message = connectionResult.toString();
        if(connectionResult!=null)
            Toast.makeText(this, connectionResult.getErrorCode(), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("positionClicked", shoppingListPositionToPlacePicker);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        shoppingListPositionToPlacePicker = savedInstanceState.getInt("positionClicked");
    }

    @Override
    public void onBackPressed(){


        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    @Override
    public void onDestroy(){
        destructListeners();
        super.onDestroy();

    }

    @Override
    public void onResume(){
        super.onResume();
    }



    private void setNavDrawer(){


        navActions = new ArrayList<>();
        navActionsImage = new ArrayList<>();

        final TextView navDrawerTextView = (TextView) findViewById(R.id.navDrawerTV);
        if(user.getUserID().length() > 4){
            navDrawerTextView.setText(user.getUserEmail());
            navDrawerTextView.setClickable(false);
        } else{
            navDrawerTextView.setText(NavDrawerConstants.SIGNIN);
            navDrawerTextView.setClickable(true);
        }



        navDrawerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(navDrawerTextView.getText().equals(NavDrawerConstants.SIGNIN)) {
                    switchToLogin();
                } else{
                    showSimpleAlertDialog("USER",user.getUserEmail()+"\n"+user.getUserName(),false);
                }
            }
        });


        navActions.add(NavDrawerConstants.POINTS);
        navActionsImage.add(NavDrawerConstants.POINTS_IMAGE);

        setUserLevel();

        if(user.getUserID().length() > 0){
            navActions.add(NavDrawerConstants.CHANGE_PASSWORD);
            navActions.add(NavDrawerConstants.SIGNOUT);

            navActionsImage.add(NavDrawerConstants.CHANGE_PASSWORD_IMAGE);
            navActionsImage.add(NavDrawerConstants.SIGNOUT_IMAGE);

        }

        navActions.add(NavDrawerConstants.EMPTY);
        navActionsImage.add(NavDrawerConstants.EMPTY);

        navActions.add(NavDrawerConstants.ABOUT);
        navActionsImage.add(NavDrawerConstants.ABOUT_IMAGE);

        navActions.add(NavDrawerConstants.RATE_US);
        navActionsImage.add(NavDrawerConstants.RATE_US_IMAGE);

        navActions.add(NavDrawerConstants.HELP);
        navActionsImage.add(NavDrawerConstants.HELP_IMAGE);

        navActions.add(NavDrawerConstants.FEEDBACK);
        navActionsImage.add(NavDrawerConstants.FEEDBACK_IMAGE);

        navDrawerLV = (ListView) findViewById(R.id.navDrawerLV);
        navDrawerAdapter = new NavDrawerListAdapter(navActions,navActionsImage,mainActivity);

        navDrawerLV.setAdapter(navDrawerAdapter);

        navDrawerLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (navActions.get(position).equals(NavDrawerConstants.SIGNOUT)) {
                    logout();
                } else if (navActions.get(position).equals(NavDrawerConstants.CHANGE_PASSWORD)) {
                    changeUserPassword();
                } else if (navActions.get(position).equals(NavDrawerConstants.POINTS) ||
                        position == 0) {
                    showUserLevel(user.getTotalPoints());
                } else if (navActions.get(position).equals(NavDrawerConstants.ABOUT)) {
                    popUpAbout();
                } else if (navActions.get(position).equals(NavDrawerConstants.RATE_US)) {
                    popUpRating();
                } else if (navActions.get(position).equals(NavDrawerConstants.HELP)) {
                    popUpHelp();
                } else if (navActions.get(position).equals(NavDrawerConstants.FEEDBACK)) {
                    popupFeedback();
                }
            }
        });

        //--set empty footer view

        navDrawerLV.addFooterView(CommonUtils.getEmptyFooterView(mainActivity));

        setNavDrawerIcons();


    }

    private void changeUserPassword(){
        if(user.getUserID().length() < 4){
            return;
        }
        LayoutInflater inflater = this.getLayoutInflater();
        View passwordResetView = (View) inflater.inflate(R.layout.reset_password_popup, null);
        final EditText passwordET = (EditText) passwordResetView.findViewById(R.id.newPassword);
        final EditText passwordRepeatET = (EditText) passwordResetView.findViewById(R.id.repeatPassword);
        final EditText currentPasswordET = (EditText) passwordResetView.findViewById(R.id.currentPassword);


        AlertDialog.Builder aisleInfoViewBuilder = new AlertDialog.Builder(this);
        AlertDialog alertDialog;

        aisleInfoViewBuilder.setView(passwordResetView); // Set an EditText view to get user input


        aisleInfoViewBuilder.setPositiveButton("Change Password", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String password1 = passwordET.getText().toString();
                String password2 = passwordRepeatET.getText().toString();
                String currentPassword = currentPasswordET.getText().toString();

                if (password1.isEmpty() || password2.isEmpty() || password1.length() < 4 || password2.length() < 4) {
                    showSimpleAlertDialog("Too Short Password", "Password must be more than 4 alphanumeric characters", true);
                } else if (password1.equals(password2)) {
                    requestPasswordChangeToFirebase(currentPassword, password2);
                } else {
                    showSimpleAlertDialog("Password Mismatch!", "New Password and Repeat password field do not match", true);
                }
            }
        });

        aisleInfoViewBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog = aisleInfoViewBuilder.show();
    }

    private void requestPasswordChangeToFirebase(String oldPassword, String newPassword){
        Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL);
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Requesting for password change....");
        pd.show();

        ref.changePassword(user.getUserEmail(), oldPassword, newPassword, new Firebase.ResultHandler() {
            @Override
            public void onSuccess() {
                try {
                    pd.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }
                showSimpleAlertDialog("Success", "Password has been changed successfully!", false);
            }

            @Override
            public void onError(FirebaseError firebaseError) {
                // error encountered
                try {
                    pd.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }
                showSimpleAlertDialog("Error", "Old password is invalid. Consider logout and \"forgot password\"", false);
            }
        });
    }

    private void showSimpleAlertDialog(String title, String message, final boolean showMethodOnOK){

        AlertDialog alertDialog = new AlertDialog.Builder(mainActivity).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            dialog.dismiss();
                        } catch(Exception e){
                            System.out.println("Dialog dismissed after activity finished....");
                        }
                        if (showMethodOnOK) {
                            changeUserPassword();
                        }
                    }
                });
        alertDialog.show();
    }

    private void setNavDrawerIcons(){


        DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        final String mActivityTitle = titleTV.getText().toString();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                titleTV.setText("Settings");
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                titleTV.setText(mActivityTitle);
                invalidateOptionsMenu();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public void setUserLevel(){

        if(user.getUserID().length()<4){
            navActions.set(0,"not signed in");
            return;
        }

        //extract new points
        final Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(!dataSnapshot.child("users/"+user.getUserID()+"/totalPoints").exists()){
                    setUserLevel();
                }
                long points = dataSnapshot.child("users/" + user.getUserID() + "/totalPoints").getValue(Long.class);
                user.setTotalPoints(points);
                navActions.set(0, UserUtils.getUserLevel(points));
                navDrawerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


    }

    public void showUserLevel(long totalPoints){
        LayoutInflater inflater = this.getLayoutInflater();
        View badgeView = inflater.inflate(R.layout.user_points_badges, null);

        int numBadges = UserUtils.getUserBadges(totalPoints);
        int badgesShown = 0;

        LinearLayout vertLayout = (LinearLayout) badgeView.findViewById(R.id.verticalLayout);

        outerloop:
        for(int i=0; i<vertLayout.getChildCount(); i++){
            LinearLayout horizLayout = (LinearLayout) vertLayout.getChildAt(i);
            for(int j=0; j<horizLayout.getChildCount();j++){
                horizLayout.getChildAt(j).setVisibility(View.VISIBLE);
                badgesShown++;
                if(badgesShown >= numBadges){
                    break outerloop;
                }

            }
        }

        String message = "You are "+navActions.get(0);
        if(navActions.get(0).equals(NavDrawerConstants.POINTS)){
            message = NavDrawerConstants.POINTS;

        }

        if(user.getUserID().length()<4){
            message = "\n\nSign in to collect points.";
        } else{
            message+="\n\nContinue collecting points helping community by adding aisles..";
        }

        message+="\n\n";

        AlertDialog.Builder builder =
                new AlertDialog.Builder(mainActivity).
                        setMessage(message).
                        setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    dialog.dismiss();
                                } catch(Exception e){
                                    System.out.println("Dialog dismissed after activity finished....");
                                }
                            }
                        }).
                        setView(badgeView);
        builder.show();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private void popUpAbout(){
        PackageInfo pInfo = null;
        String version = "-";
        int versionCode=0;
        String aboutString="";
        try {
            pInfo = mainActivity.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
            versionCode= pInfo.versionCode;
            aboutString="Developed by: ThreeCrazyMinds"+"\n\n"+
                    "Version: " + version+"\n\n"+
                    "Version Code:"+versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        CommonUtils.showSimpleAlertDialog("ABOUT", aboutString, mainActivity);


    }

    private void popUpRating(){
        Uri uri = Uri.parse("market://details?id=" + mainActivity.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + mainActivity.getPackageName())));
        }
    }

    private void popUpHelp(){
        String help="This app lets you find aisles of items in your grocery list."+"\n\n\n"
                + "1. Create your grocery lists using \"+\" at the bottom right corner. "+ "\n\n"
                + "2. Save your shopping list"+ "\n\n"
                + "3. Tap on \"Find Aisles\" button."+ "\n\n"
                + "4. Chose/Search your grocery store from the map."+ "\n\n"
                + "5. Voila!!! You should now see the aisles of your items on the right";

        CommonUtils.showSimpleAlertDialog("HELP", help, mainActivity);

    }

    private void popupFeedback(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:"
                + "threecrazyminds@gmail.com"
                + "?subject=" + "Feedback" + "&body=" + "");
        intent.setData(data);

        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);

        startActivity(intent);
    }


}