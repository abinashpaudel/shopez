package com.threecrazyminds.abinash.shopEZ;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
//import com.example.shopez.shopEZ.R;

import com.google.android.gms.common.api.GoogleApiClient;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

import database.DBInterface;
import items.ItemListsActivity;

/**
 * Created by abinash on 2/20/16.
 */
public class ShoppingOverFlowOnClickListener implements View.OnClickListener {

    Activity mainActivityContext;
    ArrayList<ShoppingList> shopList;
    int position;
    private GoogleApiClient mGoogleApiClient;
    static final int PLACE_PICKER_REQUEST = 2;

    DBInterface db;

    View view;

    public ShoppingOverFlowOnClickListener(Activity mainActivityContext,
                                           int position,
                                           ArrayList<ShoppingList> shopList,
                                           View view,
                                           GoogleApiClient mGoogleApiClient
                                           ){
        this.mainActivityContext = mainActivityContext;
        this.position = position;
        this.shopList = shopList;
        this.view = view;
        this.mGoogleApiClient = mGoogleApiClient;
        db = DBInterface.getInstance(mainActivityContext);

    }

    @Override
    public void onClick(View v) {
        PopupMenu popupMenu = new PopupMenu(mainActivityContext,v);
        popupMenu.getMenuInflater().inflate(R.menu.shop_overflow_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.shop_overflow_editName:
                        startActivity();
                        return true;
                    case R.id.start_shopping:
                        CommonUtils.startPlacePicker(mGoogleApiClient, mainActivityContext, position);
                    default:
                        return false;

                }

            }
        });

        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
            @Override
            public void onDismiss(PopupMenu menu) {
                CommonUtils.deselect(view, mainActivityContext);
            }
        });


        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Icons couldn't be showed");
        }


        popupMenu.show();
        CommonUtils.setSelectionColor(view, mainActivityContext);
    }

    private void startActivity() {

        Intent createShoppingList = new Intent(mainActivityContext, ItemListsActivity.class);

        Bundle extras = new Bundle();

        extras.putInt("position", position);
        extras.putLong("shoppingListID", shopList.get(position).getId());
        extras.putString("shoppingListName", shopList.get(position).getName());

        createShoppingList.putExtras(extras);
        mainActivityContext.startActivityForResult(createShoppingList, ShoppingListsMainActivity.REQUEST_SHOPPINGLISTNAME);
        Log.i("Position Clicked", position + "");
    }

//    private void startPlacePicker(){
//        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
//            Toast.makeText(mainActivityContext, "Enable Location Services or Check your internet connection.", Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//
//        try {
//            ShoppingListsMainActivity.shoppingListPositionToPlacePicker = position;
//            mainActivityContext.startActivityForResult(builder.build(mainActivityContext), PLACE_PICKER_REQUEST);
//        } catch (GooglePlayServicesRepairableException
//                | GooglePlayServicesNotAvailableException e) {
//
//            e.printStackTrace();
//            Toast.makeText(mainActivityContext, "Google Play Services Unavailable......", Toast.LENGTH_LONG).show();
//        }
//    }

}
