package navigationDrawer;

import android.content.pm.PackageInfo;

/**
 * Created by abinashpaudel on 4/4/16.
 */
public class NavDrawerConstants {


    public static String SIGNOUT = "Signout";
    public static final String SIGNIN="Sign In";
    public static final String CHANGE_PASSWORD = "Change Password";
    public static final String POINTS="Fetching Level...";
    public static final String HELP="Help";
    public static final String ABOUT="About";
    public static final String FEEDBACK="Feedback";
    public static final String RATE_US="Rate shopEZ";
    public static final String EMPTY="\n\n\n\n";

    public static final String SIGNIN_IMAGE="account";
    public static final String CHANGE_PASSWORD_IMAGE="change_password";
    public static final String SIGNOUT_IMAGE="account";
    public static final String POINTS_IMAGE="points";
    public static final String HELP_IMAGE="help";
    public static final String ABOUT_IMAGE="about";
    public static final String FEEDBACK_IMAGE="feedback";
    public static final String RATE_US_IMAGE="rating";


    public NavDrawerConstants(){

    }

}
