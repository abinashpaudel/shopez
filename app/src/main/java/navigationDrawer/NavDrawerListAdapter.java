package navigationDrawer;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.threecrazyminds.abinash.shopEZ.CommonUtils;
import com.threecrazyminds.abinash.shopEZ.R;

import java.util.ArrayList;

/**
 * Created by abinashpaudel on 4/4/16.
 */
public class NavDrawerListAdapter extends BaseAdapter {

    private ArrayList<String> navActionsList;
    private ArrayList<String> navActionImageList;
    private Activity context;


    public NavDrawerListAdapter(ArrayList<String> navActionsList,
                                ArrayList<String> navActionImageList,
                                Activity context){
        this.navActionsList = navActionsList;
        this.navActionImageList = navActionImageList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return navActionsList.size();
    }

    @Override
    public Object getItem(int position) {
        return navActionsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder{
        ImageView actionIV;
        TextView actionTV;
        LinearLayout navDrawerLL;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final int myPosition = position;
        final ViewHolder holder;
        View rowView = convertView;
        LayoutInflater layoutInflater = context.getLayoutInflater();
        if(rowView == null) {
            holder = new ViewHolder();
            rowView = layoutInflater.inflate(R.layout.nav_drawer_row, null);
            holder.actionIV = (ImageView) rowView.findViewById(R.id.navDrawerIV);
            holder.actionTV = (TextView) rowView.findViewById(R.id.navDrawerTV);
            rowView.setTag(holder);
        } else{
            holder = (ViewHolder) rowView.getTag();
        }


        holder.actionTV.setText(navActionsList.get(myPosition));

        if(!navActionsList.get(myPosition).equals(NavDrawerConstants.EMPTY)) {
            holder.actionIV.setVisibility(View.VISIBLE);
            holder.actionIV.setImageResource(CommonUtils.getResourceId(navActionImageList.get(myPosition), "drawable",context));
        } else{
            holder.actionIV.setVisibility(View.INVISIBLE);
        }

        return rowView;

    }
}
