package items;

import java.util.ArrayList;

/**
 * Created by abinashpaudel on 4/6/16.
 */
public class ItemList {

    private String shoppingListName;
    private ArrayList<Item> itemList;

    public String getShoppingListName() {
        return shoppingListName;
    }

    public void setShoppingListName(String shoppingListName) {
        this.shoppingListName = shoppingListName;
    }

    public ArrayList<Item> getItemList() {
        return itemList;
    }

    public void setItemList(ArrayList<Item> itemList) {
        this.itemList = itemList;
    }

    public ItemList(String shoppingListName, ArrayList<Item> itemList){
        this.shoppingListName = shoppingListName;
        this.itemList = itemList;
    }

    public ItemList(){
        shoppingListName = null;
        itemList = null;
    }
}
