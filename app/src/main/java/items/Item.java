package items;

/**
 * Created by abinash on 3/5/16.
 */
public class Item {

    private String itemName;
    private Boolean itemCheckedState;
    private String itemNote;

    public Item(String itemName,
                Boolean itemCheckedState,
                String itemNote
                ){
        this.itemName = itemName;
        this.itemCheckedState = itemCheckedState;
        this.itemNote = itemNote;
    }

    public Item(){

    }

    public String getItemName(){
        return itemName;
    }

    public void setItemName(String itemName){
        this.itemName = itemName;
    }

    public boolean getItemCheckedState() {
        return itemCheckedState;
    }

    public void setItemCheckedState(boolean itemCheckedState){
        this.itemCheckedState = itemCheckedState;
    }

    public String getItemNote() {
        return itemNote;
    }

    public void setItemNote(String itemNote){
        this.itemNote = itemNote;
    }
}
