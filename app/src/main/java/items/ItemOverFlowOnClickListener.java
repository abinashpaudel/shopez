package items;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.threecrazyminds.abinash.shopEZ.CommonUtils;
import database.DBInterface;
import com.threecrazyminds.abinash.shopEZ.R;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Created by abinash on 2/20/16.
 */
public class ItemOverFlowOnClickListener implements View.OnClickListener {
    Activity context;
    ArrayList<Item> itemList;
    int position;
    ItemsArrayAdapter itemsArrayAdapter;
    DBInterface db;
    long storeID;
    View view;
    public ItemOverFlowOnClickListener(Activity context,
                                       ArrayList<Item> itemList,
                                       int position,
                                       long storeID,
                                       ItemsArrayAdapter itemsArrayAdapter,
                                       View view){
        this.context = context;
        this.itemList = itemList;
        this.position = position;
        this.itemsArrayAdapter = itemsArrayAdapter;
        this.storeID = storeID;
        this.view = view;
        db = DBInterface.getInstance(context);


    }

    @Override
    public void onClick(View v) {
        PopupMenu popupMenu = new PopupMenu(context,v);
        popupMenu.getMenuInflater().inflate(R.menu.item_overflow_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_overflow_editName:
                        setAlertDialogOnEdit("name");
                        System.out.println("Item in edit mode.....");
                        return true;
                    case R.id.item_overflow_editQuantity:
                        setAlertDialogOnEdit("quantity");
                        System.out.println("Item Quantity in edit mode.....");
                    default:
                        return false;

                }

            }
        });

        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
            @Override
            public void onDismiss(PopupMenu menu) {
                CommonUtils.deselect(view, context);
            }
        });


        popupMenu.show();
        CommonUtils.setSelectionColor(view, context);

    }

    private void forceShowIcons(PopupMenu popupMenu){
        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Icons couldn't be showed");
        }
    }

    private void setAlertDialogOnEdit(final String type){
        String text;
        if(type.equals("name")) {
            text = itemList.get(position).getItemName();
        }
        else text = itemList.get(position).getItemNote();

        final  AlertDialog.Builder alertBuilder = new AlertDialog.Builder (context);
        final int myPosition = position;
        final AlertDialog alertDialog;
        final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        final View alertView = context.getLayoutInflater().inflate(R.layout.edit_text_popup, null);
        final EditText input = (EditText) alertView.findViewById(R.id.pwEditText);


        //get the keyboard as soon as the alert edittext pop-opens
        input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){

                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                }

            }
        });

        input.setText(text);
        input.setSelection(input.getText().toString().length()); //move the cursor to the end

        alertBuilder.setView(alertView);
        alertDialog = alertBuilder.show();

        imm.showSoftInput(input, InputMethodManager.SHOW_FORCED); //force keyboard after show

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                String newText = input.getText().toString();

                if(type.equals("name")){
                    itemList.get(position).setItemName(newText);
                }
                else{
                    itemList.get(position).setItemNote(newText);
                }

                itemsArrayAdapter.notifyDataSetChanged();
            }
        });


        //hide the keyboard on action done by triggering dismiss() which in turn adds the edited text
        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                try {
                    alertDialog.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                CommonUtils.deselect(view, context);
                return true;

            }
        });

    }
}
