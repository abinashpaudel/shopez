package items;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Point;
import android.os.Build;
import android.speech.RecognizerIntent;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.Display;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.threecrazyminds.abinash.shopEZ.CommonUtils;
import database.DBInterface;
import user.ShareListActivity;

import com.threecrazyminds.abinash.shopEZ.R;
import com.threecrazyminds.abinash.shopEZ.ShoppingListsMainActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class ItemListsActivity extends AppCompatActivity {


    private ArrayList<Item> itemList;
    private ArrayList<String> itemListAsset;
    private ArrayList<String> noteListAsset;
    private ArrayList<String> shopListAsset;

    private DBInterface db;
    ItemsArrayAdapter ad;

    private ArrayList<Boolean> userInputAisleTracker = null;


    public AutoCompleteTextView shoppingListNameET, itemNameET, noteET;

    public TableLayout itemAisleHeader;
    public ListView itemListsLV;

    private long shoppingListID;
    private String storeName;
    private String storeAddress;
    private String googlePlaceID;
    private long storeID;

    public String shoppingListName;

    final Activity context = this;
    static final int REQUEST_VOICE = 1;

    public boolean startShopping = false;

    FloatingActionButton fab;

    ActionBar actionBar;

    ShowcaseView sv;

    private void saveAndSwitchToParentActivity() {

        saveShoppingListAndItemListToDB();
        Intent home = new Intent(getApplicationContext(), ShoppingListsMainActivity.class);
        startActivity(home);
        this.finish();
        overridePendingTransition(R.transition.right_in, R.transition.right_out);
        Toast.makeText(context, shoppingListName + "saved!", Toast.LENGTH_SHORT);
    }


    private void makeToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void addItemAndNotifyAd(View v) {
        String itemName = itemNameET.getText().toString();
        String note = noteET.getText().toString();

        if (note.trim().length() == 0) {
            note = "note";
        }

        if (itemNameET.getText().toString().trim().length() == 0) {
            CommonUtils.shakeAndVibrate(context, itemNameET);
            itemNameET.setText("");
            return;
        }

        itemList.add(new Item(itemName, false, note)); //initially the item state is unchecked
        itemNameET.setText("");
        noteET.setText("");



        userInputAisleTracker.add(false); //initially the user doesn't input the aisle number.


        ad.notifyDataSetChanged();

        setProperLVHeight();
        itemListsLV.setSelection(ad.getCount() - 1);

        itemNameET.requestFocus();
        itemNameET.requestFocusFromTouch();

//        itemListsLV.smoothScrollToPosition(ad.getCount() - 1);


    }

    public void setProperLVHeight() {
        int myListViewHeight = ViewGroup.LayoutParams.WRAP_CONTENT;

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;

        View item;
        int myMaxItemsToShow = 1;
        if (ad.getCount() >= 1) {
            item = ad.getView(0, null, itemListsLV);
            item.measure(0, 0);
//            myMaxItemsToShow = (int) (getWindowHeight() / item.getMeasuredHeight() - 3);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                myMaxItemsToShow = ((int) height / item.getMeasuredHeight() - 3);
            } else{
                myMaxItemsToShow = ((int) height / item.getMeasuredHeight() - 4);
            }

            if (ad.getCount() > myMaxItemsToShow) {
                myListViewHeight = (int) ((myMaxItemsToShow+0.3) * item.getMeasuredHeight());
            }
        }

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, myListViewHeight);
        itemListsLV.setLayoutParams(params);

    }

    private int getWindowHeight(){
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        return screenHeight;
    }


    private void removeItemAndChildNotifyAd(int position) {
        itemList.remove(position);
        userInputAisleTracker.remove(position);

        ad.notifyDataSetChanged();

        setProperLVHeight();

        if (position > 5) {
            itemListsLV.setSelection(position - 1);
            itemListsLV.smoothScrollToPosition(position - 1);
        }

    }

    private boolean userInShoppingMode() {
        boolean shoppingMode = true;

        if ((storeName == null
                || storeName.length() == 0
                || storeAddress == null
                || storeAddress.length() == 0)) {
            shoppingMode = false;
        }
        return shoppingMode;
    }

    private boolean getAllItemCheckedState() {
        Boolean allChecked = true;
        if (itemList == null || itemList.size() == 0) {
            allChecked = false;
        } else {

            for (Item item : itemList) {
                if (item.getItemCheckedState() == false) {
                    allChecked = false;
                    break;
                }
            }

        }
        return allChecked;
    }


    private String setShoppingListName() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyy HH:mm:ss");

        //If there are items but no shopping List name, get time stamp
        if (itemList.size() != 0 && shoppingListNameET.getText().toString().trim().length() == 0) {
            SimpleDateFormat sdfSN = new SimpleDateFormat("dd-MMM HH:mm:ss");
            String currentDateandTime = sdfSN.format(new Date());
            shoppingListNameET.setText(currentDateandTime);

        }
        return shoppingListNameET.getText().toString();
    }



    private void extractItemListsArray() {


        shoppingListID = CommonUtils.getShoppingListID(context);
        shoppingListName = CommonUtils.getShopListName(context);
        storeName = CommonUtils.getStoreName(context);
        storeAddress=CommonUtils.getStoreAddress(context);
        googlePlaceID = CommonUtils.getGooglePlaceId(context);

        storeID = db.getStoreId(googlePlaceID, storeName, storeAddress);
        itemList = db.getItemList(shoppingListID);

        if (itemList == null) {
            itemList = new ArrayList<Item>();
            userInputAisleTracker = new ArrayList<>();
        } else {
            userInputAisleTracker = new ArrayList<Boolean>(Collections.nCopies(itemList.size(),false));
        }

    }

    private void extractFromSharedPreferences() {

        shoppingListID = CommonUtils.getShoppingListID(context);

        ArrayList<Item> myItemList = CommonUtils.getItemLists(context);
        ArrayList<Enum> myThumbList = CommonUtils.getThumbList(context);
        ArrayList<Boolean> myUserInputAisleTracker = CommonUtils.getUserInputAisleTracker(context);


        if (myItemList != null) {
            itemList.clear();
            for (int i = 0; i < myItemList.size(); i++) {
                itemList.add(myItemList.get(i));
            }
        }

        if (myThumbList != null) {
            userInputAisleTracker.clear();
            for (int i = 0; i < myThumbList.size(); i++) {
                userInputAisleTracker.add(myUserInputAisleTracker.get(i));
            }
        }

        ad.notifyDataSetChanged();

    }

    private void saveToSharedPreferences() {

        CommonUtils.saveShoppingListInfo(shoppingListName,shoppingListID,storeName,storeAddress,googlePlaceID, context);
        CommonUtils.storeItemList(itemList, context);
        CommonUtils.storeUserInputAisleTracker(userInputAisleTracker, context);

    }

    private void removeFromSharedPreferences() {
        CommonUtils.removeItemLists(context);
        CommonUtils.removeThumbList(context);
        CommonUtils.removeUserInputAisleTracker(context);
    }

    private void saveShoppingListAndItemListToDB() {
        //set shopping list name
        String myShoppingListName = setShoppingListName();

        boolean done = getAllItemCheckedState();

        shoppingListID = CommonUtils.saveShoppingListAndItemListToDB(myShoppingListName,
                shoppingListID,done,itemList,context);
        /*
        //save shopping List Name
        shoppingListID = db.addOrUpdateShoppingListName(myShoppingListName, shoppingListID, new Date().getTime());

        if (shoppingListID < 0) {
            Toast.makeText(this, "Couldn't save shopping list", Toast.LENGTH_SHORT);
        }


        //check to see if all items are marked as done
        Boolean done = getAllItemCheckedState();
        long errorWhileSavingShoppingListState = db.saveShoppingListState(shoppingListID, done);

        if (errorWhileSavingShoppingListState < 0) {
            Toast.makeText(this, "Couldn't save item lists", Toast.LENGTH_SHORT);
        }

        db.saveItems(shoppingListID, itemList);
        db.saveThumbState(storeID, itemList, thumbsState);
        */


    }


    //Save the instance before destroy
    @Override
    protected void onDestroy() {

        super.onDestroy();
        ad.destructListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        saveToSharedPreferences();
        super.onStop();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_item_lists);
        } else {
            setContentView(R.layout.activity_item_lists_landscape);
        }

        initializeUI();

    }

    @Override
    public void onBackPressed() {

        saveAndSwitchToParentActivity();
        super.onBackPressed();
    }

    @Override
    public void onResume() {

        super.onResume();

        return;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {

        super.onPostCreate(savedInstanceState);
    }

//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//
//
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedState) {
//
//        super.onRestoreInstanceState(savedState);
//        extractFromSharedPreferences();
//        setProperLVHeight();
//        removeFromSharedPreferences();
//
//
//    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        saveShoppingListAndItemListToDB();
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_item_lists_landscape);

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_item_lists);

        }
        initializeUI();
        setProperLVHeight();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_item_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();
        if (id == android.R.id.home) {
            saveAndSwitchToParentActivity();
            return true;
        } //return back to home

        //if delete is chosen 1) Set dialog 2) Delete all checked items
        else if (id == R.id.action_delete) {

            new AlertDialog.Builder(this)
                    .setTitle("Confirm Deletion")
                    .setMessage("Are you sure? All checked items will be deleted")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {

                            deleteCheckedItemsFromLast();
                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        }

        else if(id==R.id.startShopping){
            startShopping = true;
            saveAndSwitchToParentActivity();

        }

        return true;
    }

    private void deleteCheckedItemsFromLast() {
        boolean atleastOneItemToDel = false;

        if (itemList != null) {
            for (int i = itemList.size() - 1; i >= 0; i--) {
                if (itemList.get(i).getItemCheckedState()) {
                    removeItemAndChildNotifyAd(i);
                    atleastOneItemToDel = true;
                }

            }

        }
        if(atleastOneItemToDel == false){
            Toast.makeText(context,"No items to delete....",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){

        super.onCreateContextMenu(menu,v,menuInfo);
        CommonUtils.hideKeyboard(context,itemNameET);
        CommonUtils.hideKeyboard(context, noteET);
    }


    private void initializeUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        TextView titleTV = null;
        ImageView shareBut = (ImageView) findViewById(R.id.btn_share);
        shareBut.setVisibility(View.VISIBLE);

        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true); // show or hide the default home button
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
            actionBar.setDisplayShowTitleEnabled(true);
            titleTV = (TextView) findViewById(R.id.toolbar_title);
        }

        shareBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveShoppingListAndItemListToDB();
                Intent shareListActivity = new Intent(getApplicationContext(), ShareListActivity.class);
                startActivity(shareListActivity);
                overridePendingTransition(R.transition.left_in, R.transition.right_out);
            }
        });


        //Initialize
        itemNameET = (AutoCompleteTextView) findViewById(R.id.itemNameET);
        setPopAnimation(itemNameET,1000);


        itemListAsset = CommonUtils.getAssetListFromFile("item_name_lists_asset.txt", context);
        itemNameET.setThreshold(1);
        ArrayAdapter<String> itemAssetAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line, itemListAsset);

        itemNameET.setAdapter(itemAssetAdapter);


        noteET = (AutoCompleteTextView) findViewById(R.id.noteET);
        setPopAnimation(noteET, 1000);


        noteListAsset = CommonUtils.getAssetListFromFile("note_item_assets.txt", context);
        noteET.setThreshold(1);
        ArrayAdapter<String> noteAssetAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line,noteListAsset);
        noteET.setAdapter(noteAssetAdapter);

        fab = (FloatingActionButton) findViewById(R.id.fab);

//        micButton = (ImageButton) findViewById(R.id.micButton);
//        micButton.setVisibility(View.GONE);

        itemListsLV = (ListView) findViewById(R.id.itemListsLV);
        itemListsLV.setTranscriptMode(ListView.TRANSCRIPT_MODE_DISABLED);
        itemListsLV.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState !=0){ //up and down = 1
                    CommonUtils.hideKeyboard(context, itemNameET);
                    CommonUtils.hideKeyboard(context, noteET);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

//        myRL = (RelativeLayout) findViewById(R.id.mainRL);


        shoppingListNameET = (AutoCompleteTextView) findViewById(R.id.shoppingListNameET);



        shopListAsset = CommonUtils.getAssetListFromFile("shop_name_lists_asset.txt", context);
        ArrayAdapter<String> shopAssetAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line,shopListAsset);
        shoppingListNameET.setAdapter(shopAssetAdapter);

        shoppingListNameET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    CommonUtils.hideKeyboard(context, shoppingListNameET);
                    itemNameET.requestFocus();
                    itemNameET.requestFocusFromTouch();
                    return true;

                }

                return false;
            }
        });


        itemNameET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if ((itemNameET.getText().length() != 0)) {
                        addItemAndNotifyAd(null);
                    } else {
                        CommonUtils.hideKeyboard(context, itemNameET);
                    }
                }
                return true;
            }
        });

        itemNameET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sv != null) {
                    sv.hide();
                }
            }
        });

        shoppingListNameET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sv!=null){
                    CommonUtils.setFirstTimeActivityToFalse(CommonUtils.ITEM_LIST_ACTIVITY,context);
                    sv.hide();
                }
            }
        });


        noteET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (itemNameET.getText().length() == 0 && noteET.getText().length() != 0) {
                        CommonUtils.shakeAndVibrate(context, itemNameET);
                        return true;
                    } else if (itemNameET.getText().length() == 0 && noteET.getText().length() == 0) {
                        CommonUtils.hideKeyboard(context, itemNameET);
                        return true;
                    }

                    if ((itemNameET.getText().length() != 0)) {
                        addItemAndNotifyAd(null);
                    }
                }
                return true;
            }
        });


        itemNameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    fab.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green)));
                    fab.setImageResource(R.drawable.uparrow_white_48);
//                    itemListsLV.setStackFromBottom(true);
                } else {
                    fab.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorAccent)));
                    fab.setImageResource(R.drawable.save);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

//        micButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startVoiceRecognitionActivity();
//            }
//        });

        db = DBInterface.getInstance(context);

        extractItemListsArray();


        itemAisleHeader = (TableLayout) findViewById(R.id.itemAisleHeaderTL);


        if (!shoppingListName.equals("")) {
            shoppingListNameET.setText(shoppingListName);
        } //set the name of the correct shopping List Name

        if (shoppingListNameET.getText().length() == 0) {
            shoppingListNameET.requestFocus();
            shoppingListNameET.requestFocusFromTouch();
            setPopAnimation(shoppingListNameET, 500);
        } else {
            itemNameET.requestFocus();
            itemNameET.requestFocusFromTouch();
            shoppingListNameET.clearAnimation();
        }


        itemListsLV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                removeItemAndChildNotifyAd(position);

                return true;
            }
        });

        if (userInShoppingMode()) {
            titleTV.setText("@" + storeName);
        } else {
            titleTV.setText("items");
        }


        ad = new ItemsArrayAdapter(this,
                itemList,
                storeName,
                storeAddress,
                storeID,
                googlePlaceID,
                itemNameET);
        itemListsLV.setAdapter(ad);

        setProperLVHeight();


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (itemNameET.getText().toString().trim().length() != 0) {
                    addItemAndNotifyAd(view);
                } else {
                    saveAndSwitchToParentActivity();
                }

            }
        });

        itemListsLV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                itemList.remove(position);
                ad.notifyDataSetChanged();
                setProperLVHeight();
                return true;
            }
        });

        registerForContextMenu(itemNameET);
        registerForContextMenu(noteET);

        showTutorialForItemList();



    }

    private void showTutorialForItemList(){

        if(CommonUtils.activityStartedFirst(CommonUtils.ITEM_LIST_ACTIVITY,context) == false
                || itemList.size()>0){
            return;
        }


        sv = new ShowcaseView.Builder(context)
                .setTarget(new ViewTarget(R.id.itemNameET,this))
                .setContentTitle("Add desired Items to your List")
                .setContentText("Optionally set the title of your list")
                .hideOnTouchOutside()
                .build();

        sv.hideButton();
        sv.setHideOnTouchOutside(true);


        sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
            @Override
            public void onShowcaseViewHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                CommonUtils.setFirstTimeActivityToFalse(CommonUtils.ITEM_LIST_ACTIVITY,context);
            }

            @Override
            public void onShowcaseViewShow(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {


            }
        });



    }



    private void startVoiceRecognitionActivity() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Item Name");
        startActivityForResult(intent, REQUEST_VOICE);
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_VOICE && resultCode == RESULT_OK) {
            // Populate the wordsList with the String values the recognition engine thought it heard
            ArrayList<String> matches = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);

            String myName = "";
            for (String word : matches) {
                myName = word + " ";
            }

            if(myName.length()>0) {
                if(itemNameET.hasFocus()){
                    itemNameET.setText(myName);
                } else if(noteET.hasFocus()){
                    noteET.setText(myName);
                }
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setPopAnimation(View view, long delayInMillis ){
        Animation popAnimation = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
        AnimationSet animationSet = new AnimationSet(false);
//        animationSet.addAnimation(shopPulse);
        animationSet.addAnimation(popAnimation);
        animationSet.setStartOffset(delayInMillis);
        view.startAnimation(animationSet);
    }


}
