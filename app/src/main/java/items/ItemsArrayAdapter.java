package items;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.threecrazyminds.abinash.shopEZ.CommonUtils;


import aisle.AisleListAdapter;
import database.DBInterface;

import com.threecrazyminds.abinash.shopEZ.R;
import com.threecrazyminds.abinash.shopEZ.ShoppingList;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import database.FireBaseItem;
import database.FireBaseItemInfo;
import database.FireBaseUtils;
import user.User;

public class ItemsArrayAdapter extends BaseAdapter {


    Activity context;
    ArrayList<Item> itemList;
    List<String> overFlowActions;
    ArrayAdapter<String> overFlowAdapter;
    String groceryStoreName = null;
    String groceryStoreAddress = null;
    String googlePlaceID = null;
    long storeID;
    DBInterface db = null;
    private ArrayList<FireBaseItemInfo> itemInfos;
    private ArrayList<Boolean> userAisleInputTracker;

    private ArrayList<FireBaseItem> fireBaseItems = null;
    private ArrayList<String> fireBaseItemKeys = null;

    final private String OFFLINE = "%OFFLINE%";
    final private String CONNECTING = "%CONNECTING%";
    final private String CONNECTED = "%CONNECTED%";
    final private String CONNECTING_GREEN = "%CONNECTED_GREEN%";

    private String connectionMode = CONNECTING;

    private ArrayList<ValueEventListener> aislePullListeners;
    private ArrayList<Firebase> pullconnectedRefs;

    private ArrayList<ValueEventListener> aislePushListeners;
    private ArrayList<Firebase> pushConnectedRefs;

    private EditText itemNameET;

    //sort comparator from latest to oldest
    Comparator<Item> itemListComparator;


    public ItemsArrayAdapter(Activity activityContext,
                             ArrayList<Item> itemList,
                             String groceryStoreName,
                             String groceryStoreAddress,
                             long storeID,
                             final String googlePlaceID,
                             EditText itemNameET) {

        this.itemList = itemList;
        this.groceryStoreName = groceryStoreName;
        this.groceryStoreAddress = groceryStoreAddress;
        this.storeID = storeID;
        context = activityContext;
        overFlowActions = Arrays.asList("Delete", "Edit");
        overFlowAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_activated_1, overFlowActions);
        db = DBInterface.getInstance(context);
        this.googlePlaceID = googlePlaceID;
        this.itemNameET = itemNameET;


        fireBaseItems = new ArrayList<FireBaseItem>();
        fireBaseItemKeys = new ArrayList<String>();

        aislePullListeners = new ArrayList<ValueEventListener>();
        pullconnectedRefs = new ArrayList<Firebase>();

        aislePushListeners = new ArrayList<ValueEventListener>();
        pushConnectedRefs = new ArrayList<Firebase>();

        itemListComparator = new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {
                boolean b1 = o1.getItemCheckedState();
                boolean b2 = o2.getItemCheckedState();
                if( b1 && !b2 ) {
                    return +1;
                }
                if( ! b1 && b2 ) {
                    return -1;
                }
                return 0;
            }
        };


    }

    @Override
    public int getCount() {

        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

//    @Override
//    public int getViewTypeCount() {
//        return 2;
//    }

    public class ViewHolder {
        CheckBox cb;
        TextView itemNameTV;
        TextView itemNoteTV;
        LinearLayout aisleInfoLayout;
        ImageView offlineIV;
        ImageView addAisleIV;
        FrameLayout aisleFrameLayout;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View rowView = convertView;
        LayoutInflater layoutInflater = context.getLayoutInflater();
        if (rowView == null) {
            holder = new ViewHolder();
            rowView = layoutInflater.inflate(R.layout.item_card_view, null);
            holder.itemNameTV = (TextView) rowView.findViewById(R.id.rowText);
            holder.itemNoteTV = (TextView) rowView.findViewById(R.id.rowNote);

            holder.cb = (CheckBox) rowView.findViewById(R.id.rowCheckBox);

            holder.offlineIV = (ImageView) rowView.findViewById(R.id.offlineImage);
            holder.aisleInfoLayout = (LinearLayout) rowView.findViewById(R.id.aisleInfoLL);
            holder.addAisleIV = (ImageView) rowView.findViewById(R.id.addAisleButton);

            holder.aisleFrameLayout = (FrameLayout) rowView.findViewById(R.id.aisleInfoFrame);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.itemNameTV.setText(itemList.get(position).getItemName());
        holder.itemNoteTV.setText(itemList.get(position).getItemNote());
        holder.cb.setChecked(itemList.get(position).getItemCheckedState());

        holder.cb.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.cb.isChecked()) {
                    itemList.get(position).setItemCheckedState(true);
                } else {
                    itemList.get(position).setItemCheckedState(false);
                }
                setTextColor(holder);
                Collections.sort(itemList, itemListComparator);
                notifyDataSetChanged();

            }
        });


        holder.itemNameTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showItemNameNoteInfo(position);
            }
        });

        holder.itemNameTV.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });

        holder.itemNoteTV.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });

        holder.aisleInfoLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });

        holder.aisleFrameLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });

        holder.aisleInfoLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                itemList.remove(position);
                notifyDataSetChanged();
                return true;
            }
        });

        holder.itemNoteTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showItemNameNoteInfo(position);
            }
        });


        //changed the checked position text to somewhat blue to indicate that the item has been marked as done
        setTextColor(holder);
        Collections.sort(itemList,itemListComparator);


        if (!userInShoppingMode()) {

            resetHolder(holder, View.GONE);
            return rowView;
        }


        itemInfos = getItemInfos(position);

        /*----BEGIN SHOPPING MODE----*/
        resetHolder(holder, View.INVISIBLE);

        //if aisleInfo is empty add the "+" button and animate it if connection is successfull.
        if (itemInfos == null || itemInfos.size() == 0) {

            if (connectionMode.equals(CONNECTING)) {
                holder.offlineIV.setImageResource(R.drawable.connecting);
                holder.offlineIV.setVisibility(View.VISIBLE);
                holder.aisleInfoLayout.setVisibility(View.GONE);
                holder.addAisleIV.setVisibility(View.GONE);
                holder.offlineIV.setColorFilter(ContextCompat.getColor(context, R.color.inactiveColor));

            } else if(connectionMode.equals(CONNECTING_GREEN)){
                holder.offlineIV.setImageResource(R.drawable.connecting);
                holder.offlineIV.setVisibility(View.VISIBLE);
                holder.aisleInfoLayout.setVisibility(View.GONE);
                holder.addAisleIV.setVisibility(View.GONE);
                holder.offlineIV.setColorFilter(ContextCompat.getColor(context, R.color.colorAccent));

            } else if (connectionMode.equals(OFFLINE)) {
                holder.offlineIV.setImageResource(R.drawable.cellular_off);
                holder.offlineIV.setVisibility(View.VISIBLE);
                holder.aisleInfoLayout.setVisibility(View.GONE);
                holder.addAisleIV.setVisibility(View.GONE);
                holder.offlineIV.setColorFilter(ContextCompat.getColor(context, R.color.inactiveColor));

            } else if (connectionMode.equals(CONNECTED)) {
                holder.aisleInfoLayout.setVisibility(View.GONE);
                holder.offlineIV.setVisibility(View.GONE);
                holder.addAisleIV.setVisibility(View.VISIBLE);
            }
        } else {
            holder.aisleInfoLayout.setVisibility(View.VISIBLE);
            ((TextView) holder.aisleInfoLayout.getChildAt(0)).setVisibility(View.GONE);
            ((TextView) holder.aisleInfoLayout.getChildAt(1)).setVisibility(View.GONE);
            ((TextView) holder.aisleInfoLayout.getChildAt(2)).setVisibility(View.GONE);

            for (int i = 0; i < itemInfos.size(); i++) {

                ((TextView) holder.aisleInfoLayout.getChildAt(i)).setVisibility(View.VISIBLE);
                String itemInfo = itemInfos.get(i).getAisle();
                if(itemInfo.length() == 0){
                    itemInfo = itemInfos.get(i).getSection();
                }

                ((TextView) holder.aisleInfoLayout.getChildAt(i)).setText(itemInfo);
            }

            holder.offlineIV.setVisibility(View.GONE);
            holder.addAisleIV.setVisibility(View.GONE);
        }


        holder.aisleInfoLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDetailedAisleInfo(position);
            }
        });

        holder.addAisleIV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDetailedAisleInfo(position);
            }
        });

        return rowView;

    }


    private void removeItemAndNotify(int position){
        itemList.remove(position);
        notifyDataSetChanged();
    }

    private void setTextColor(ViewHolder vh) {
        if (vh.cb.isChecked()) {
            vh.itemNameTV.setTextColor(Color.rgb(189, 189, 189));
            vh.itemNoteTV.setTextColor(Color.rgb(189, 189, 189));

            vh.itemNameTV.setPaintFlags(vh.itemNameTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            vh.itemNoteTV.setPaintFlags(vh.itemNoteTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            vh.itemNameTV.setTextColor(Color.BLACK);
            vh.itemNoteTV.setTextColor(Color.BLACK);

            vh.itemNameTV.setPaintFlags(vh.itemNameTV.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            vh.itemNoteTV.setPaintFlags(vh.itemNoteTV.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);


        }
    }

    private boolean userInShoppingMode() {
        if (groceryStoreName == null ||
                groceryStoreName.equals("") ||
                groceryStoreAddress == null ||
                groceryStoreAddress.equals("")) {
            return false;
        }
        return true;
    }


    private void resetHolder(ViewHolder vh, int visibility) {
        vh.aisleInfoLayout.setVisibility(visibility);
        vh.offlineIV.setVisibility(visibility);
        vh.addAisleIV.setVisibility(visibility);
    }


    private void toSize(ImageView iv, int width, int height) {
        int myHeight = convertDpToPixel(height, context);
        int myWidth = convertDpToPixel(width, context);
        android.view.ViewGroup.LayoutParams layoutParams = iv.getLayoutParams();
        layoutParams.width = myHeight;
        layoutParams.height = myWidth;
        iv.setLayoutParams(layoutParams);

    }

    private int convertDpToPixel(int dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

    private void showItemNameNoteInfo(int position) {
        final String itemName = itemList.get(position).getItemName();
        final String note = itemList.get(position).getItemNote();
        final int myPosition = position;

        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        final AlertDialog alertDialog;
        final View alertView = context.getLayoutInflater().inflate(R.layout.itemame_itemnote_popup, null);

        final EditText itemNamePopup = (EditText) alertView.findViewById(R.id.itemNameET_popup);
        final EditText notePopup = (EditText) alertView.findViewById(R.id.noteET_popup);

        itemNamePopup.setText(itemName);
        notePopup.setText(note);

        itemNamePopup.setSelection(itemNamePopup.getText().toString().length()); //move cursor to end
        alertBuilder.setView(alertView);


        alertBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String myNewItemName = itemNamePopup.getText().toString().trim();
                String myNewItemNote = notePopup.getText().toString().trim();
                if (myNewItemName.length() <= 0) {
                    Toast.makeText(context, itemName + " deleted..", Toast.LENGTH_SHORT);
                    itemList.remove(myPosition);

                } else {

                    itemList.get(myPosition).setItemName(myNewItemName);
                    itemList.get(myPosition).setItemNote(myNewItemNote);
                }
                notifyDataSetChanged();
                CommonUtils.hideKeyboard(context, itemNameET);

            }
        });

        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                CommonUtils.hideKeyboard(context, itemNameET);
            }
        });


        alertDialog = alertBuilder.create();

        alertDialog.show();

        ((ViewGroup)alertDialog.getWindow().getDecorView())
                .getChildAt(0).startAnimation(AnimationUtils.loadAnimation(
                context, R.anim.zoom_in));


        alertDialog.setCanceledOnTouchOutside(true);


    }

    private void popupDetailedAisleInfo(final int position) {
        //Abinash
        final String clickedItemNameL = itemList.get(position).getItemName().toLowerCase();
        LayoutInflater inflater = context.getLayoutInflater();
        View aisleInfoView = (View) inflater.inflate(R.layout.aisle_info_popup, null);
        View addButtonView = (View) inflater.inflate(R.layout.aisle_list_footer_view, null);
        ListView aisleListView = (ListView) aisleInfoView.findViewById(R.id.aisleListView);
        aisleListView.addFooterView(addButtonView); //setButton

        TextView addAisleButton = (TextView) addButtonView.findViewById(R.id.addAisleButton);

        AlertDialog.Builder aisleInfoViewBuilder = new AlertDialog.Builder(context);
        AlertDialog alertDialog;

        aisleInfoViewBuilder.setView(aisleInfoView); // Set an EditText view to get user input
        TextView titleView = (TextView) aisleInfoView.findViewById(R.id.item_name);
        titleView.setText(itemList.get(position).getItemName());

        int clickedFireBaseItemIndex = findItemInLocalFireBase(clickedItemNameL);
        final FireBaseItem clickedFireBaseItem = fireBaseItems.get(clickedFireBaseItemIndex);


        final AisleListAdapter aisleListAdapter = new AisleListAdapter(context,
                clickedFireBaseItem.getItemInfos(),
                googlePlaceID,
                itemList.get(position).getItemName(),
                this,
                addAisleButton);

        aisleListView.setAdapter(aisleListAdapter);

        addAisleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aisleListAdapter.popupUserAisleInput(getItemInfos(position).size(), "");
            }
        });


        aisleInfoViewBuilder.setPositiveButton("Send Feedback", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.out.println("Sending Feedback...");
                ArrayList<Enum> myThumbsState = aisleListAdapter.getThumbsState();
                boolean aisleInfoChanged = false;
                for (int i = 0; i < myThumbsState.size(); i++) {
                    clickedFireBaseItem.likeOrDislikeAnAisle(i, myThumbsState.get(i));
                    if (myThumbsState.contains(CommonUtils.Thumb.UP) || myThumbsState.contains(CommonUtils.Thumb.DOWN)) {
                        aisleInfoChanged = true;
                    }
                }
                if (aisleInfoChanged) {
                    saveToFireBase(clickedItemNameL, false);
                    Toast.makeText(context, "Thanks for the feedback!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Nothing to send..", Toast.LENGTH_SHORT).show();
                }
                notifyDataSetChanged();
            }
        });

        aisleInfoViewBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                notifyDataSetChanged();
            }
        });

        alertDialog = aisleInfoViewBuilder.create();

        alertDialog.show();

        ((ViewGroup)alertDialog.getWindow().getDecorView())
                .getChildAt(0).startAnimation(AnimationUtils.loadAnimation(
                context, R.anim.bounce));

    }


    private void pullItemInfoFromServer(String itemName, final boolean notifyAdapterAfterPull) {

        final String itemNameL = itemName.toLowerCase().trim();
        Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL + "/items/" + googlePlaceID);
        Query queryRef = ref.orderByChild("itemName").equalTo(itemNameL);

        queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot querySnapShot) {
                System.out.println("Query with itemName = " + itemNameL + " resulted in " + querySnapShot.getChildrenCount() + " children");
                System.out.println("querySnapShot address --> " + querySnapShot.getRef().toString());
                for (DataSnapshot itemKey : querySnapShot.getChildren()) {
                    System.out.println("Child Key = " + itemKey.getKey().toString());
                    int fbItemIndex = findItemInLocalFireBase(itemNameL);

                    FireBaseItem fireBaseItem = querySnapShot.child(itemKey.getKey().toString()).getValue(FireBaseItem.class);
                    System.out.println("Aisle info for child" + itemNameL + " --> " + fireBaseItem.getItemInfos().get(0).getAisle());

                    //remove oldFireBaseItem, add newFireBaseItem
                    fireBaseItems.set(fbItemIndex, fireBaseItem);
                    //remove emptykey, add emptyKey
                    fireBaseItemKeys.set(fbItemIndex, itemKey.getKey().toString());

                }

                connectionMode = CONNECTED;
                System.out.println("Connection mode is CONNECTED... while pulling item from server");

                if (notifyAdapterAfterPull) {
                    notifyDataSetChanged();
                } else{
                    System.out.println("Couldn't nofity data set");
                }
            }


            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    private ArrayList<FireBaseItemInfo> getItemInfos(int rowPosition) {
        //if item is in localFirebase, get it from localFirebase, else, pull and store in local Database
        final String myItemNameL = itemList.get(rowPosition).getItemName().trim().toLowerCase();

        int fbItemIndex = findItemInLocalFireBase(myItemNameL);
        if (fbItemIndex >= 0) { //itemFound
            return fireBaseItems.get(fbItemIndex).getItemInfos();

        } else { //If item is not found in LocalDB add an Empty Item, then pullFromServer and update when available

            if(CommonUtils.isNetworkAvailable(context) == false){
                connectionMode = OFFLINE;
                return null;
            }


            // if connection is not offline, then add an Empty Item
            FireBaseItem emptyFireBaseItem = new FireBaseItem(myItemNameL);
            fireBaseItems.add(emptyFireBaseItem);
            fireBaseItemKeys.add("");

            //check Connection State
            connectionMode = CONNECTING;
            final Firebase connectedRef = new Firebase(FireBaseUtils.FIREBASE_URL + "/.info/connected");

            ValueEventListener valueEventListener = connectedRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    boolean connected = snapshot.getValue(Boolean.class);
                    if (connected) {
                        connectionMode = CONNECTING_GREEN;
                        System.out.println("Connection is true, pulling items from server");
                        pullItemInfoFromServer(myItemNameL, true);
                    } else {
                        System.out.println("Connecting...");
                        connectionMode = CONNECTING;
                    }
                    notifyDataSetChanged();
                }

                @Override
                public void onCancelled(FirebaseError error) {
                    System.err.println("Listener was cancelled");
                }
            });

            aislePullListeners.add(valueEventListener);
            pullconnectedRefs.add(connectedRef);

        }

        return null;

    }

    public void saveToFireBase(final String itemName, final boolean givePointsToUser) {

        final int fbItemIndex = findItemInLocalFireBase(itemName);

        final Firebase connectedRef = new Firebase(FireBaseUtils.FIREBASE_URL + "/.info/connected");
        if(CommonUtils.isNetworkAvailable(context) == false){
            Toast.makeText(context,"Cannot save to the cloud in an offline mode",Toast.LENGTH_SHORT).show();
        }

        ValueEventListener valueEventListener = connectedRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    updateItemInfoToFireBaseOnConnectionTrue(fbItemIndex, givePointsToUser);
                }
            }

            @Override
            public void onCancelled(FirebaseError error) {
                Toast.makeText(context, "Connection cancelled.", Toast.LENGTH_SHORT).show();
            }
        });

        aislePushListeners.add(valueEventListener);
        pushConnectedRefs.add(connectedRef);

    }

    public void saveToLocalFireBase(final String itemName, final String itemAisle,
                                    final String itemComment,
                                    final String itemSection,
                                    final String itemSubSection
                                    ) {
        final int fbItemIndex = findItemInLocalFireBase(itemName);
        String userID = CommonUtils.getUser(context).getUserID();
        if (userID.equals("")){
            Toast.makeText(context,"Thank you! Login to collect points!",Toast.LENGTH_SHORT).show();
        }
        fireBaseItems.get(fbItemIndex).addNewAisleInfo(itemAisle, itemComment, itemSection, itemSubSection, userID);
        notifyDataSetChanged();
    }

    private int findItemInLocalFireBase(String itemName) {
        String itemNameL = itemName.toLowerCase();
        int itemPosInFDB = -1;
        for (int i = 0; i < fireBaseItems.size(); i++) {
            if (fireBaseItems.get(i).getItemName().equals(itemNameL)) {
                itemPosInFDB = i;
            }
        }
        return itemPosInFDB;
    }

    private void updateItemInfoToFireBaseOnConnectionTrue(final int fbItemIndex, final boolean givePointsToUser) {
        //checkFirebase
        final Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL);
        final String itemNameL = fireBaseItems.get(fbItemIndex).getItemName();

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final DataSnapshot dataSnapShot = dataSnapshot;


                User user = CommonUtils.getUser(context);
                String userID = user.getUserID();
                if(user == null){
                    System.out.println("User is null. Can't give points......");
                }
                if(userID.length() !=0){
                    if(givePointsToUser && user!=null){
                        addAndGivePointsToUser(user);
                    }
                }
                //add google place ID if it doesn't exist
                if (!dataSnapShot.child("stores/" + googlePlaceID).exists()) {
                    dataSnapShot.child("stores/" + googlePlaceID + "/storeName").getRef().setValue(groceryStoreName);
                    dataSnapShot.child("stores/" + googlePlaceID + "/storeAddress").getRef().setValue(groceryStoreAddress);
                }

                if (fireBaseItemKeys.get(fbItemIndex).length() <= 1) { //no item in the firebase cloud, push
                    Firebase refTemp = dataSnapShot.child("items/" + googlePlaceID).getRef().push();
                    refTemp.setValue(fireBaseItems.get(fbItemIndex));
                    //update emptyKey to real key
                    fireBaseItemKeys.set(fbItemIndex, refTemp.getKey());

                    System.out.println("Item info was nothing.... hence pushed");
                } else { //itemIs in the firebase, overwrite the old item
                    System.out.println("Item key is-->" + fireBaseItemKeys.get(fbItemIndex));
                    dataSnapShot.child("items/" + googlePlaceID + "/" + fireBaseItemKeys.get(fbItemIndex)).getRef().setValue(fireBaseItems.get(fbItemIndex));
                    System.out.println("Item was already there..... hence updated");

                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Toast.makeText(context, "Cannot connect to the network....", Toast.LENGTH_SHORT).show();
            }
        });

        notifyDataSetChanged();

    }

    public void destructListeners() {
        //Destroy all firebase listeners
        for (int i = 0; i < aislePullListeners.size(); i++) {
            pullconnectedRefs.get(i).removeEventListener(aislePullListeners.get(i));
        }

        pullconnectedRefs = null; //garbage cleanup
        aislePullListeners = null;


        for (int i = 0; i < aislePushListeners.size(); i++) {
            pushConnectedRefs.get(i).removeEventListener(aislePushListeners.get(i));

        }

        pushConnectedRefs = null; //garbage collection
        aislePushListeners = null;
    }

    public void addAndGivePointsToUser(final User user){

        //extract new points
        final Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //add userID if it doesn't exist
                if (!dataSnapshot.child("users/" + user.getUserID()).exists()) {
                    dataSnapshot.child("users/" + user.getUserID()).getRef().setValue(user);
                }

                System.out.println(dataSnapshot.child("users/"+user.getUserID()+"/totalPoints").getRef().toString());
                long points = dataSnapshot.child("users/"+user.getUserID()+ "/totalPoints").getValue(Long.class);
                long newPoints = points+1;

                //update to new points
                user.setTotalPoints(newPoints);

                dataSnapshot.child("users/" + user.getUserID()).getRef().setValue(user);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


    }

}
