package database;



import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

import com.threecrazyminds.abinash.shopEZ.CommonUtils;
import com.threecrazyminds.abinash.shopEZ.ShoppingList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import items.Item;

/**
 * Created by Saaantosh Tripatthi on 2/19/2016.
 */
public class DBInterface {

    //===========================================================
    //  Field constants
    //===========================================================

    private static DBInterface dbInstance = null;
    innerSQLiteOpenHelper openHelperObject;
    SQLiteDatabase dbReadable;
    SQLiteDatabase dbWritable;

    private static final int DB_VERSION = 2;
    private static final String DB_NAME = "db_aisle_finder_local";

    //--Table Name Variables
    private static final String TABLE_USER = "user_table";
    private static final String TABLE_ITEM_TO_COMMENT = "table_item_to_comment";
    private static final String TABLE_ITEM_TO_AISLE_DATA="table_data_to_track_item_to_aisle_association";
    private static final String TABLE_ITEM_TO_AISLE="table_item_to_aisle";
    private static final String TABLE_STORES_AND_IDS="table_list_of_stores_and_ids";
    private static final String TABLE_ITEMS_TO_SHOPPING_LIST="table_list_of_items_in_shopping_lists";
    private static final String TABLE_SHOPPING_LISTS="table_list_of_shopping_lists";
    private static final String TABLE_ITEM_TO_ITEMID="table_item_name_to_id";

    private static ArrayList<String> listOfTables;

    //--------- EmailID and password table with USERID-------------------
    private static final String KEY_USER_EMAIL = "userEmail";
    private static final String KEY_USER_PSSWD = "userPassword";
    private static final String KEY_USER_NAME = "userName";
    private static final String KEY_USER_ID = "userID";

    //sort comparator from latest to oldest
    Comparator<ShoppingList> shoppingListComparator = new Comparator<ShoppingList>() {
        @Override
        public int compare(ShoppingList o1, ShoppingList o2) {
            if(o1.getId() == o2.getId()){
                return 0;
            }
            if(o1.getId() < o2.getId()){
                return 1;
            }

            return -1;
        }
    };


    public static DBInterface getInstance(Context context){
        if(dbInstance == null){
            dbInstance = new DBInterface(context);
        }
        return dbInstance;
    }

    private DBInterface(Context context){
        listOfTables = new ArrayList<String>();
        listOfTables.add(TABLE_USER);
        listOfTables.add(TABLE_ITEM_TO_COMMENT);
        listOfTables.add(TABLE_ITEM_TO_AISLE_DATA);
        listOfTables.add(TABLE_ITEM_TO_AISLE);
        listOfTables.add(TABLE_STORES_AND_IDS);
        listOfTables.add(TABLE_ITEMS_TO_SHOPPING_LIST);
        listOfTables.add(TABLE_SHOPPING_LISTS);
        listOfTables.add(TABLE_ITEM_TO_ITEMID);

        openHelperObject=new innerSQLiteOpenHelper(context, DB_NAME, null, DB_VERSION);
        SQLiteDatabase dbReadable=openHelperObject.getReadableDatabase();
        SQLiteDatabase dbWritable = openHelperObject.getWritableDatabase();

        if(dbInstance!=null){
            System.out.println("DB Already initiated....");
        }


    }

    private void createWritableDB(){
        if(dbWritable==null){
            dbWritable=openHelperObject.getWritableDatabase();
        }
    }

    private void createReadableDB(){
        if(dbReadable==null){
            dbReadable=openHelperObject.getReadableDatabase();
        }
    }




    /*==============================================================================================
    //
   ITEMS AND IDS
   COLUMNS: field_item_id	field_item_name	field_date_time_added
   TABLE_ITEM_TO_ITEMID
    ============================================================================================*/
    public long addItem(String itemName){
        createWritableDB();
        long itemId=getItemId(itemName);
        if(itemId!=-1){
            return itemId;
        }
        long returnCode;
        ContentValues values=new ContentValues();

        values.put("field_item_name", itemName);
        values.put("field_date_time_added", new Date().getTime());
        try{
            returnCode=dbWritable.insertOrThrow(TABLE_ITEM_TO_ITEMID, null, values);
        }catch(SQLException exception){
            returnCode=-1;
        }
        return returnCode;
    }


    public long getItemId(String itemName){
        long val = -1;
        createReadableDB();
        long itemId;
        String queryStringFindShoppingListName="SELECT field_item_id FROM " + TABLE_ITEM_TO_ITEMID+ " WHERE field_item_name=?";
        String[] selectionArgs={String.valueOf(itemName)};
        Cursor cursor=dbReadable.rawQuery(queryStringFindShoppingListName, selectionArgs);
        if(cursor.moveToNext()){
            val = cursor.getLong(0);
        }
        cursor.close();
        return val;
    }


    //--- if item is in the db it gets the itemId, if the item is not in the db, it adds the
    //item to the db and gets the itemId
    //-1 means error; otherwise correct itemId
    private long addItemGetItem(String itemName){
        long itemId;
        itemId=getItemId(itemName);
        if(itemId==-1){
            itemId=addItem(itemName);
        }
        return itemId;
    }





    public String getItemName(long itemId){
        String val = "-1";
        createReadableDB();
        String itemName;
        String queryStringFindItemName="SELECT field_item_name FROM " + TABLE_ITEM_TO_ITEMID+ " WHERE field_item_id=?";
        String[] selectionArgs={String.valueOf(itemId)};
        Cursor cursor=dbReadable.rawQuery(queryStringFindItemName, selectionArgs);
        if(cursor.moveToNext()){
            val =  cursor.getString(0);
        }
        cursor.close();
        return val;
    }







    /*==============================================================================================
    //
   ITEMS IN SHOPPING LIST
   COLUMNS: field_shopping_list_id	field_item_id	field_is_item_checked	field_is_item_deleted	field_items_order_in_the_list	field_date_time_added
   TABLE_ITEMS_TO_SHOPPING_LIST
    ============================================================================================*/

    public ArrayList<ArrayList<String>> getShoppingListItem(long shoppingListId){
        //---AR: change name getItemsOfShoppingId
        //--
        createReadableDB();

        Cursor resultCursor;
        String shoppingListName=getShoppingListNameFromId(shoppingListId);
        if(shoppingListName.contentEquals("-1")){ //requested shopping list does not exist
            long val=-1;
            return null;
        }

        String queryStringGetShoppingListItems="SELECT field_shopping_list_id,  field_item_id, field_is_item_checked FROM " +  TABLE_ITEMS_TO_SHOPPING_LIST + " WHERE field_shopping_list_id=?";
        String[] selctArgs={String.valueOf(shoppingListId)};
        resultCursor=dbReadable.rawQuery(queryStringGetShoppingListItems, selctArgs);

        int numberOfLists=resultCursor.getCount();
        ArrayList<ArrayList<String>> resultList=new ArrayList<ArrayList<String>>(numberOfLists);
        String itemName;
        while(resultCursor.moveToNext()){
            itemName=getItemName(resultCursor.getLong(1));
            ArrayList<String> newList=new ArrayList<String>(3);
            newList.add(0, String.valueOf(resultCursor.getInt(0))); //
            newList.add(1, itemName);//item name
            newList.add(2, String.valueOf(resultCursor.getInt(2)));//
            resultList.add(newList);
        }
        return resultList;
    }


    public long saveShoppingListItem(long shoppingListId, ArrayList<ArrayList<String>> itemList){
        // itemList -> name, checked,

        //------ AR: ArrayList of items and ArrayList of checked status

        createWritableDB();
        createReadableDB();
        int numOfElements;
        numOfElements=itemList.size();
        String shoppingListName=getShoppingListNameFromId(shoppingListId);
        if(shoppingListName.contentEquals("-1")){
            long val=-1;
            return val;
        }

        Cursor resultCursor;
        long itemId;
        long returnCode=-2;
        ContentValues values=new ContentValues();
        ArrayList<String> item;
        for(int i=0;i<numOfElements;i++){
            item=itemList.get(i);
            itemId=getItemId(item.get(0));
            if(itemId==-1) itemId=addItem(item.get(0));
            String queryStringCheckIfItemExists="SELECT * FROM " +TABLE_ITEMS_TO_SHOPPING_LIST + " WHERE field_shopping_list_id=? AND field_item_id=?";
            String[] selctArgs={String.valueOf(shoppingListId), String.valueOf(itemId)};
            resultCursor=dbReadable.rawQuery(queryStringCheckIfItemExists, selctArgs);


            //public int update (String table, ContentValues values, String whereClause, String[] whereArgs)


            values.put("field_shopping_list_id", shoppingListId);
            values.put("field_item_id", itemId);
            values.put("field_is_item_deleted", 0);
            values.put("field_items_order_in_the_list", i);
            values.put("field_is_item_checked", item.get(1));
            values.put("field_date_time_added", 0);

            if (resultCursor.moveToNext()){ // if the item already exists update it
                String whereClause="field_shopping_list_id=? AND field_item_id=?";
                int numOfRows=dbWritable.update(TABLE_ITEMS_TO_SHOPPING_LIST, values, whereClause,  selctArgs);
                returnCode=numOfRows;
            }else{ //if the item does not exist then add it
                try{
                    returnCode=dbWritable.insertOrThrow(TABLE_ITEMS_TO_SHOPPING_LIST, null, values);
                }catch(SQLException exception){
                    returnCode=-1;
                }
            }
        }

        return returnCode;
    }



    public long saveItems(long shoppingListId, ArrayList<Item> itemList){
        //--- return shoppingListId if successful, -1 if not successful

        //TABLE_ITEMS_TO_SHOPPING_LIST
        //field_item_in_shopping_list_id	field_shopping_list_id	field_item_id
        // field_is_item_checked	field_is_item_deleted	field_items_order_in_the_list
        // field_quantity_for_the_item	field_date_time_added
        if(dbWritable==null){
            dbWritable=openHelperObject.getWritableDatabase();
        }
        if(dbReadable==null){
            dbReadable=openHelperObject.getReadableDatabase();
        }

        long returnValue=shoppingListId;
        int numberOfRowsDeleted;
        int numberOfElements;
        Item item;
        long itemId;
        String itemName;
        int checkedStatus;
        ContentValues values=new ContentValues();

        //assume that the shopping list exists; if not, return -1
        //if the input itemList is null; delete the items in the list in the db and return error
        //
        if(isActiveShoppingList(shoppingListId)==-1){
            returnValue=-1;
        }else{

            // delete existing items in the shopping list
            String whereClause="field_shopping_list_id=?";
            String[] whereArgs={String.valueOf(shoppingListId)};
            numberOfRowsDeleted=dbWritable.delete(TABLE_ITEMS_TO_SHOPPING_LIST, whereClause, whereArgs);

            if(itemList==null)  {
                returnValue=-1;
                return returnValue;
            }

            //add the given items
            numberOfElements=itemList.size();
            for(int i=0;i<numberOfElements;i++){
                values.clear();
                item=itemList.get(i);
                itemName=item.getItemName();
                values.put("field_shopping_list_id", shoppingListId);
                itemId=addItemGetItem(itemName);
                if(itemId==-1){
                    returnValue= -1;
                    break;
                }
                values.put("field_item_id", itemId);
                checkedStatus = item.getItemCheckedState() ? 1 : 0;
                values.put("field_is_item_checked", checkedStatus);
                values.put("field_is_item_deleted", 0);
                values.put("field_items_order_in_the_list", i);
                values.put("field_quantity_for_the_item", item.getItemNote());
                values.put("field_date_time_added", 0);
                try{
                    long rowIdOfNewRow=dbWritable.insertOrThrow(TABLE_ITEMS_TO_SHOPPING_LIST, null, values);
                }catch(SQLException exception){
                    returnValue=-1;
                }
            }
        }
        return returnValue;

    }


    public ArrayList<Item> getItemList(long shoppingListId){
        //Given shopping list ID, returns the list of items in the shopping list
        //Order of the returned item is the same as field_items_order_in_the_list
        //If shopping list Id doesn't exist or the shopping list is empty null is returned
        //Make up of the table:
        //TABLE_ITEMS_TO_SHOPPING_LIST
        //field_item_in_shopping_list_id	field_shopping_list_id	field_item_id
        // field_is_item_checked	field_is_item_deleted	field_items_order_in_the_list
        // field_quantity_for_the_item	field_date_time_added
        if(dbWritable==null){
            dbWritable=openHelperObject.getWritableDatabase();
        }
        if(dbReadable==null){
            dbReadable=openHelperObject.getReadableDatabase();
        }
        ArrayList<Item> itemList=null;
        Item item;
        String itemName;
        Boolean itemCheckedState;
        String itemQuantity;

        String queryStringGetItemList=
                "SELECT field_item_id, field_is_item_checked,  field_quantity_for_the_item FROM " + TABLE_ITEMS_TO_SHOPPING_LIST + " WHERE field_shopping_list_id=? "+
                        " ORDER BY field_items_order_in_the_list ASC";
        String[] selectionArgs={String.valueOf(shoppingListId)};
        Cursor resultCursor=dbReadable.rawQuery(queryStringGetItemList, selectionArgs);
        int numberOfRows=resultCursor.getCount();
        if(numberOfRows>0){
            itemList =new ArrayList<Item>(numberOfRows);
            int i=0;
            while(resultCursor.moveToNext()){
                long itemName2=resultCursor.getLong(0);
                itemName=getItemName(resultCursor.getLong(0));//item id from cursor, item name from itemId
                itemCheckedState=resultCursor.getInt(1)==1 ? true:false; //true=1 is checked; false=0 is unchecked
                itemQuantity=resultCursor.getString(2);
                item=new Item(itemName, itemCheckedState, itemQuantity);
                itemList.add(item);
                i=i+1;
            }
        }
        return itemList;
    }

    /*==============================================================================================
    //
   NAME OF SHOPPING LISTS
   COLUMNS: field_shopping_list_id	field_shopping_list_name	field_date_time_added	field_is_shopping_list_checked	field_is_shopping_list_deleted
   TABLE_SHOPPING_LISTS
    ============================================================================================*/
    public long saveShoppingLists(ArrayList<ShoppingList> listOfShoppingLists){
        createWritableDB();
        createReadableDB();

        int numberOfRowsDeleted; int numberOfShoppingLists; ShoppingList shoppingList;
        String shoppingListName; long shoppingListId; String whereClause; String[] whereArgs;
        int isCheckedInt; long returnValue;
        int numberOfRowsUpdated;

        returnValue=-1; //change if successful

        //delete existing shopping list (set all the shopping list's field_is_shopping_list_deleted to 1
        ContentValues values=new ContentValues();
        values.put("field_is_shopping_list_deleted", 1);
        whereClause=null; whereArgs=null;
        numberOfRowsDeleted=dbWritable.update(TABLE_SHOPPING_LISTS, values, whereClause, whereArgs);

        // update the shopping  lists
        numberOfShoppingLists=listOfShoppingLists.size();
        for(int i=0; i<numberOfShoppingLists; i++){
            values.clear();
            shoppingList=listOfShoppingLists.get(i);
            shoppingListId=shoppingList.getId();
            if(doesShoppingListIdExistsDeletedOrNot(shoppingListId)){
                values.put("field_shopping_list_name", shoppingList.getName());
                isCheckedInt=shoppingList.getDone()? 1:0;
                values.put("field_is_shopping_list_checked", isCheckedInt);
                values.put("field_is_shopping_list_deleted", 0);
                whereClause="field_shopping_list_id=?";
                whereArgs=new String[1];
                whereArgs[0]=String.valueOf(shoppingListId);
                numberOfRowsUpdated=dbWritable.update(TABLE_SHOPPING_LISTS, values, whereClause, whereArgs);
                returnValue=numberOfRowsUpdated==1? returnValue+numberOfRowsUpdated:returnValue;
            }
        }
        return returnValue;
    }

    public ArrayList<ShoppingList> getShoppingLists(){
        createReadableDB();

        ArrayList<ShoppingList> listOfShoppingLists=null;
        ContentValues values=new ContentValues();
        String[] selectionArgs=null; Cursor resultCursor;
        int numberOfRecords; int i;
        ShoppingList shoppingList; String shoppingListName;
        Boolean isListChecked; long shoppingListId;

        String queryStringGetActiveShoppingLists=
                "SELECT field_shopping_list_id, field_shopping_list_name, field_is_shopping_list_checked, field_date_time_added"+
                        " FROM "+ TABLE_SHOPPING_LISTS +
                        " WHERE field_is_shopping_list_deleted=?"+
                        " ORDER BY field_date_time_added ASC";
        selectionArgs= new String[1];selectionArgs[0]=String.valueOf(0);
        resultCursor=dbReadable.rawQuery(queryStringGetActiveShoppingLists, selectionArgs);
        numberOfRecords=resultCursor.getCount();
        if(numberOfRecords>0){
            listOfShoppingLists=new ArrayList<ShoppingList>();
            while(resultCursor.moveToNext()){
                shoppingListId=resultCursor.getLong(0);//id of the shopping list
                shoppingListName=resultCursor.getString(1);//name of the shopping list
                isListChecked=resultCursor.getInt(2)==1?true:false;

                shoppingList=new ShoppingList(shoppingListName, isListChecked);
                shoppingList.setId(shoppingListId);
                listOfShoppingLists.add(shoppingList);
            }
        }
        if(listOfShoppingLists!=null) {
            Collections.sort(listOfShoppingLists, shoppingListComparator);
        }
        return listOfShoppingLists;
    }


    private boolean doesShoppingListIdExistsDeletedOrNot(long shoppingListId){
        createReadableDB();
        String queryStringCheckIfShoppingListChecked="SELECT field_shopping_list_id FROM "+ TABLE_SHOPPING_LISTS +" WHERE field_shopping_list_id=? ";
        String[] selectionArgs={String.valueOf(shoppingListId)};
        Cursor resultCursor=dbReadable.rawQuery(queryStringCheckIfShoppingListChecked, selectionArgs);
        boolean resultCode=resultCursor.getCount()==1? true:false;
        return resultCode;
    }

    public long addShoppingList(String shoppingListName, double dateTimeCreated){
        String message;
        long returnCode=-1;

        createWritableDB();
        createReadableDB();

        if(shoppingListName.length()==0 || shoppingListName==null){
            return returnCode;
        }

        String queryStringCheckIfShoppingListExists="SELECT field_shopping_list_id FROM " + TABLE_SHOPPING_LISTS + " WHERE field_shopping_list_name=? AND field_date_time_added=?";
        String[] selectionArgs={shoppingListName, Double.toString(dateTimeCreated)};
        Cursor resultCursor=dbReadable.rawQuery(queryStringCheckIfShoppingListExists, selectionArgs);
        if(resultCursor.moveToNext()){ // if shopping list already exists do not bother
            long val=0;
            return val;
        }
        ContentValues values=new ContentValues();
        values.put("field_shopping_list_name", shoppingListName);
        values.put("field_date_time_added", dateTimeCreated);
        values.put("field_is_shopping_list_checked", 0);
        values.put("field_is_shopping_list_deleted", 0);
        try{
            returnCode=dbWritable.insertOrThrow(TABLE_SHOPPING_LISTS, null, values);
        }catch(SQLException exception){
            returnCode=-1;
        }
        return returnCode;
    }
    //added 2016-03-03; name changed 2016-03-04
    //if shopping list id is there, update name;
    // if shopping list is not there add the shopping list
    // return value is valid shoppingListId always, -1 if could not
    public long addOrUpdateShoppingListName(String shoppingListName, long shoppingListId, double dateTimeCreated){
        long returnValue;
        long addedListId;
        long numberOfRowsAffected;
        //check if shopping list Id exists
        if(isActiveShoppingList(shoppingListId)==-1){// id doesn't exist -- make the shopping list
            addedListId=addShoppingList(shoppingListName, dateTimeCreated);
            returnValue= (addedListId==-1)?-1:addedListId;
        }else{
            //-- id exist
            if(shoppingListName.length()==0 || shoppingListName==null){
                returnValue=deleteShoppingList(shoppingListId);
            }else{
                numberOfRowsAffected=changeShoppingListName(shoppingListId, shoppingListName);
                returnValue= (numberOfRowsAffected!=1 || numberOfRowsAffected==-1) ? -1:shoppingListId;
            }
        }
        return returnValue;
    }
    //added 2016-03-03 (no test; not to be called from outside)
    private long changeShoppingListName(long shoppingListId, String shoppingListName){
        createWritableDB();

        ContentValues values=new ContentValues();
        values.put("field_shopping_list_name",shoppingListName);
        String whereClause="field_shopping_list_id=?";
        String[] selectArgs={String.valueOf(shoppingListId)};
        int numOfRows=dbWritable.update(TABLE_SHOPPING_LISTS, values, whereClause, selectArgs);
        return numOfRows;
    }
    //added 2016-03-04
    // assumes that the id exists; check to be done at the calling point
    private long deleteShoppingList(long shoppingListId){
        //COLUMNS: field_shopping_list_id	field_shopping_list_name	field_date_time_added	field_is_shopping_list_checked	field_is_shopping_list_deleted
        //TABLE_SHOPPING_LISTS
        if(dbWritable==null){
            dbWritable=openHelperObject.getWritableDatabase();
        }
        long returnValue;
        ContentValues values=new ContentValues();
        values.put("field_is_shopping_list_deleted",1);
        String whereClause="field_shopping_list_id=?";
        String[] selectArgs={String.valueOf(shoppingListId)};
        int numberOfRowsAffected=dbWritable.update(TABLE_SHOPPING_LISTS, values, whereClause, selectArgs);
        returnValue= (numberOfRowsAffected!=1 || numberOfRowsAffected==-1) ? -1:shoppingListId;

        //==============
        // To do delete the items associated with the shopping list. currently, only way to reach those items through
        // shopping list Id.
        //================
        return returnValue;
    }
    private long isActiveShoppingList(long shoppingListId){
        createReadableDB();
        Cursor resultCursor;
        String queryStringCheckIfItemExists="SELECT * FROM " + TABLE_SHOPPING_LISTS +" WHERE field_shopping_list_id=? AND field_is_shopping_list_deleted=?";
        String[] selctArgs={String.valueOf(shoppingListId), String.valueOf(0)};
        resultCursor=dbReadable.rawQuery(queryStringCheckIfItemExists, selctArgs);
        if(resultCursor.moveToNext()){
            return shoppingListId;
        }else{
            return -1;
        }
    }
    public String getShoppingListNameFromId(long shoppingListId)  {
        String val = "-1";
        createReadableDB();
        String queryStringFindShoppingListName="SELECT field_shopping_list_name FROM " + TABLE_SHOPPING_LISTS + " WHERE field_shopping_list_Id=?";

        String[] selectionArgs={String.valueOf(shoppingListId)};
        Cursor cursor=dbReadable.rawQuery(queryStringFindShoppingListName, selectionArgs);

        if(cursor.moveToNext()){
            val =  cursor.getString(0);
        }
        cursor.close();
        return val;
    }
    public ArrayList<ArrayList<String>> getShoppingLists(long temp){
        createReadableDB();
        String queryStringGetShoppingListAndId=
                "SELECT field_shopping_list_id, field_shopping_list_name, field_is_shopping_list_checked"+
                        " FROM " + TABLE_SHOPPING_LISTS +
                        " WHERE field_is_shopping_list_deleted=0";
        Cursor resultCursor=dbReadable.rawQuery(queryStringGetShoppingListAndId, null);
        int numberOfLists=resultCursor.getCount();
        ArrayList<ArrayList<String>> resultList=new ArrayList<ArrayList<String>>(numberOfLists);

        while(resultCursor.moveToNext()){
            ArrayList<String> newList=new ArrayList<String>(3);
            newList.add(0, String.valueOf(resultCursor.getLong(0)));  //id
            newList.add(1, resultCursor.getString(1)); //name
            newList.add(2, String.valueOf(resultCursor.getInt(2))); //checked or not
            resultList.add(newList);
        }
        return resultList;
    }
    //added 2016-03-01
    //COLUMNS: field_shopping_list_id	field_shopping_list_name	field_date_time_added	field_is_shopping_list_checked	field_is_shopping_list_deleted
    //TABLE_SHOPPING_LISTS
    public long saveShoppingListState(long shoppingListId, boolean checkedState){
        createWritableDB();
        int checkedStatus = checkedState ? 1 : 0;
        ContentValues values=new ContentValues();
        values.put("field_is_shopping_list_checked", checkedStatus);
        String whereClause="field_shopping_list_id=? ";
        String[] selectArgs={String.valueOf(shoppingListId)};
        int numOfRows=dbWritable.update(TABLE_SHOPPING_LISTS, values, whereClause, selectArgs);
        long returnCode=numOfRows;
        return returnCode;
    }
    public long getShoppingListState(long shoppingListId){
        createReadableDB();
        String queryStringCheckIfShoppingListChecked="SELECT field_is_shopping_list_checked FROM " + TABLE_SHOPPING_LISTS + " WHERE field_shopping_list_id=? ";
        String[] selectionArgs={String.valueOf(shoppingListId)};
        Cursor resultCursor=dbReadable.rawQuery(queryStringCheckIfShoppingListChecked, selectionArgs);
        long returnCheckedState;
        if(resultCursor.moveToNext()){
            returnCheckedState=resultCursor.getLong(0);
        }else{
            returnCheckedState=-1;
        }

        return returnCheckedState;
    }



    /*==============================================================================================
    // public int update (String table, ContentValues values, String whereClause, String[] whereArgs)
   STORE LIST
   COLUMNS: field_store_id	field_store_name	field_street_number	field_street_name	field_zipcode	field_state_name	field_country_name
   TABLE_STORES_AND_IDS
    ============================================================================================*/

    private long getStoreId(String storeName, String streetNumbrer, String streetName, String zipCode, String stateName, String countryName){

        long val = -1;
        createReadableDB();

        String queryStringFindStoreId="SELECT field_store_id FROM " + TABLE_STORES_AND_IDS + " WHERE"+
                " field_store_name=? AND field_street_number=? AND field_street_name=? AND field_zipcode=? AND field_state_name=? AND field_country_name=?";
        String[] selectionArgs={storeName, streetNumbrer, streetName, zipCode, stateName, countryName};
        Cursor cursor=dbReadable.rawQuery(queryStringFindStoreId, selectionArgs);
        if(cursor.moveToNext()){
            val =  cursor.getLong(0);
        }
        cursor.close();
        return val;
    }
    private long addStore(String storeName, String streetNumber, String streetName, String zipCode, String stateName, String countryName){
        createWritableDB();
        long storeId=getStoreId(storeName, streetNumber, streetName, zipCode, stateName, countryName);
        if(storeId!=-1){
            return storeId;
        }
        long returnCode;
        ContentValues values=new ContentValues();
        values.put("field_store_name", storeName);
        values.put("field_street_number", streetNumber);
        values.put("field_street_name", streetName);
        values.put("field_zipcode", zipCode);
        values.put("field_state_name", stateName);
        values.put("field_country_name", countryName);
        try{
            returnCode=dbWritable.insertOrThrow(TABLE_STORES_AND_IDS, null, values);
        }catch(SQLException exception){
            returnCode=-1;
        }
        return returnCode;
    }

    public long addStore(String placeId, String storeName, String addressString){
        //if not added, return -1. If added return thee store id.
        createWritableDB();
        long storeId=returnStoreId(placeId);
        if(storeId!=-1){
            return storeId;
        }
        if(storeName.length()==0 || addressString.length()==0){
            return -1;
        }
        long returnCode;
        ContentValues values=new ContentValues();
        values.put("field_store_name", storeName);
        values.put("field_store_address_string", addressString);
        values.put("field_place_id", placeId);

        try{
            returnCode=dbWritable.insertOrThrow(TABLE_STORES_AND_IDS, null, values);
            returnCode=returnStoreId(placeId);
        }catch(SQLException exception){
            returnCode=-1;
        }
        return returnCode;
    }


    private long returnStoreId(String placeId){
        long val = -1;
        createReadableDB();

        String queryStringFindStoreId="SELECT field_store_id FROM " + TABLE_STORES_AND_IDS + " WHERE"+
                " field_place_id=?";
        String[] selectionArgs={placeId};
        Cursor cursor=dbReadable.rawQuery(queryStringFindStoreId, selectionArgs);
        if(cursor.moveToNext()){
            val =  cursor.getLong(0);
        }
        cursor.close();
        return val;


    }



    public long getStoreId(String placeId, String storeName, String addressString){
        long storeId;

        createReadableDB();
        if(placeId == null ||
                storeName == null ||
                addressString == null ||
                storeName.length() ==0 ||
                placeId.length()==0){
            storeId= -1;
        }else{
            storeId=returnStoreId(placeId);
            if(storeId==-1){
                storeId=addStore(placeId, storeName, addressString);
            }
        }
        return storeId;
    }

    /*==============================================================================================
    // public int update (String table, ContentValues values, String whereClause, String[] whereArgs)
   ITEM TO AISLE
   COLUMNS: field_entry_id	field_item_id	field_store_id	field_aisle_number
   TABLE_ITEM_TO_AISLE
    ============================================================================================*/

    public String getItemAisle(String itemName, long storeId){
        long itemId=getItemId(itemName);
        return getItemAisle(itemId, storeId);
    }
    public String getItemAisle(long itemId, long storeId){
        String val = "";
        createReadableDB();

        String queryStringFindStoreId="SELECT field_aisle_number FROM "+ TABLE_ITEM_TO_AISLE + " WHERE"+
                " field_item_id=? AND field_store_id=? ";
        String[] selectionArgs={Long.toString(itemId), Long.toString(storeId)};
        Cursor cursor=dbReadable.rawQuery(queryStringFindStoreId, selectionArgs);
        if(cursor.moveToNext()){
            val =  cursor.getString(0);
        }
        cursor.close();
        return val;
    }
    public long setItemAisle(long itemId, long storeId, String aisleNumber){
        createWritableDB();
        if(getItemName(itemId).contentEquals("-1")){ // if id is not present do not add; return -1; 2015-03-01
            return -1;
        }


        String aisleNumberOrig=getItemAisle(itemId, storeId);
        ContentValues values=new ContentValues();
        values.put("field_item_id", itemId);
        values.put("field_store_id", storeId);
        values.put("field_aisle_number", aisleNumber);
        long returnCode;
        if (!aisleNumberOrig.contentEquals("")){ // if the item already exists update it
            String whereClause="field_item_id=? AND field_store_id=?";
            String[] selectArgs={Long.toString(itemId), Long.toString(storeId)};
            int numOfRows=dbWritable.update(TABLE_ITEM_TO_AISLE, values, whereClause,  selectArgs);
            returnCode=numOfRows;
        }else{ //if the item does not exist then add it
            try{
                returnCode=dbWritable.insertOrThrow(TABLE_ITEM_TO_AISLE, null, values);
            }catch(SQLException exception){
                returnCode=-1;
            }
        }
        return returnCode;
    }
    public long setItemAisle(String itemName, long storeId, String aisleNumber){
        long itemId=addItemGetItem(itemName);
        return setItemAisle(itemId, storeId, aisleNumber);

    }
    public ArrayList<String> getAisleInfo(long storeId, ArrayList<Item> itemArrayList){
        int numberOfItems=itemArrayList.size();
        String itemName="";
        String aisleNumber="";
        Item item;
        ArrayList<String> aisleInfo=new ArrayList<String>(numberOfItems);
        for(int i=0; i<numberOfItems; i++){
            item=itemArrayList.get(i);
            itemName=item.getItemName();
            aisleNumber=getItemAisle(itemName, storeId);
            aisleInfo.add(i, aisleNumber);
        }
        return aisleInfo;
    }



    /*=================================================================================================
      ITEM TO COMMENT
      TABLE_ITEM_TO_COMMENT
      field_entry_id	field_item_id	field_store_id	field_item_comment	field_item_comment_date_time_updated

     ==================================================================================================*/
    public long setItemComment(String itemName, long storeId, String comment){
        long itemId=addItemGetItem(itemName);
        return setItemComment(itemId, storeId, comment);

    }
    public long setItemComment(long itemId, long storeId, String comment){
        createWritableDB();
        if(getItemName(itemId).contentEquals("-1")){ // if id is not present do not add; return -1; 2015-03-01
            return -1;
        }


        String commentOriginal=getItemComment(itemId, storeId);
        ContentValues values=new ContentValues();
        values.put("field_item_id", itemId);
        values.put("field_store_id", storeId);
        values.put("field_item_comment", comment);
        values.put("field_item_comment_date_time_updated", new Date().getTime());
        long returnCode;
        if (!commentOriginal.contentEquals("")){ // if the item already exists update it
            String whereClause="field_item_id=? AND field_store_id=?";
            String[] selectArgs={Long.toString(itemId), Long.toString(storeId)};
            int numOfRows=dbWritable.update(TABLE_ITEM_TO_COMMENT, values, whereClause,  selectArgs);
            returnCode=numOfRows;
        }else{ //if the item does not exist then add it
            try{
                returnCode=dbWritable.insertOrThrow(TABLE_ITEM_TO_COMMENT, null, values);
            }catch(SQLException exception){
                returnCode=-1;
            }
        }
        return returnCode;
    }
    public String getItemComment(String itemName, long storeId){
        long itemId=getItemId(itemName);
        return getItemComment(itemId, storeId);
    }
    public String getItemComment(long itemId, long storeId){
        String val = "";
        createReadableDB();

        String queryStringFindStoreId="SELECT field_item_comment FROM " + TABLE_ITEM_TO_COMMENT + " WHERE"+
                " field_item_id=? AND field_store_id=? ";
        String[] selectionArgs={Long.toString(itemId), Long.toString(storeId)};
        Cursor cursor=dbReadable.rawQuery(queryStringFindStoreId, selectionArgs);
        if(cursor.moveToNext()){
            val =  cursor.getString(0);
        }
        cursor.close();
        return val;
    }




/*==============================================================================================
// public int update (String table, ContentValues values, String whereClause, String[] whereArgs)
ITEM TO AISLE TRACKER
COLUMNS: field_entry_id	field_item_id	field_store_id	field_aisle_number	field_check_thumb	field_date_time_added
TABLE_ITEM_TO_AISLE_DATA
============================================================================================*/

    //private long addUserFeedbackToAisleTracker(String itemName, String storeName, ){

    //}
    private long addUserFeedbackToAisleTracker(long itemId, long storeId, int checkThumb){
        createWritableDB();


        ContentValues values=new ContentValues();
        String aisleNumber=getItemAisle(itemId, storeId);
        if (aisleNumber.length()==0){
            return -1;
        }
        values.put("field_item_id", itemId);
        values.put("field_store_id", storeId);
        values.put("field_aisle_number", aisleNumber);
        values.put("field_check_thumb", checkThumb);
        values.put("field_date_time_added", String.valueOf(new Date().getTime()));
        long returnCode;
        try{
            returnCode=dbWritable.insertOrThrow(TABLE_ITEM_TO_AISLE_DATA, null, values);
        }catch(SQLException exception){
            returnCode=-1;
        }

        return returnCode;
    }


    public long saveThumbState(long storeId, ArrayList<Item> itemNameList, ArrayList<Enum> thumbStateList){
        if(itemNameList == null || thumbStateList == null){
            return -1;
        }
        int numberOfItems=itemNameList.size();
        String itemName="";
        Enum thumbState= CommonUtils.Thumb.UNKNOWN;
        int thumbStateInt=0;
        long itemId;
        long returnCode;
        for(int i=0;i<numberOfItems;i++){
            itemName=itemNameList.get(i).getItemName();
            thumbState=thumbStateList.get(i);
            itemId=getItemId(itemName);
            if(itemId==-1){
                continue;
            }
            if(thumbState == CommonUtils.Thumb.NEUTRAL){
                thumbStateInt=0;
            }else if(thumbState == CommonUtils.Thumb.UP){
                thumbStateInt=1;
            }else if(thumbState == CommonUtils.Thumb.DOWN){
                thumbStateInt=-1;
            }else{
                continue;
            }
            addUserFeedbackToAisleTracker(itemId, storeId, thumbStateInt);
        }
        returnCode=1;
        return returnCode;
    }

    /**
     * Checks to see if the email exists. If email exists return userID, else return -1
     * @param email Email of the user that was provided to the database to check
     */
    public long getUserIDFromEmailAndPassword(String email,String password){
        long val = -1;
        createReadableDB();
        String query_FindUserID="SELECT " + KEY_USER_ID + " FROM " + TABLE_USER
                + " WHERE " + KEY_USER_EMAIL + "=? AND "+KEY_USER_PSSWD + "=?";
        String[] selectionArgs={String.valueOf(email), String.valueOf(password)};
        Cursor cursor=dbReadable.rawQuery(query_FindUserID, selectionArgs);
        if(cursor.moveToNext()){
            val =  cursor.getLong(0);
        }
        cursor.close();
        return val;
    }

    public String getUserName(long userID){
        String val = "";
        createReadableDB();
        String query_FindUserName="SELECT " + KEY_USER_NAME + " FROM " + TABLE_USER
                + " WHERE " + KEY_USER_ID + "=?";
        String[] selectionArgs = {String.valueOf(userID)};
        Cursor cursor = dbReadable.rawQuery(query_FindUserName,selectionArgs);
        if(cursor.moveToNext()){
            val =  cursor.getString(0);
        }

        cursor.close();
        return val;

    }


    /**
     * Checks to see if email exists. This is to uniquely identify an email
     * @param email email prvoided
     * @return true if email exists in the database, false otherwise
     */
    public boolean ifEmailExists(String email){
        boolean val = false;
        createReadableDB();

        String query_FindUserID="SELECT " + KEY_USER_ID + " FROM " + TABLE_USER
                + " WHERE " + KEY_USER_EMAIL + "=?";
        String[] selectionArgs={String.valueOf(email)};
        Cursor cursor = dbReadable.rawQuery(query_FindUserID,selectionArgs);
        if(cursor.moveToNext()){
            val = true;
        }

        cursor.close();
        return val;
    }


    /**
     * Adds the email, password and name of the user to the table
     * Returns the userID if transaction is successfull, -1 if unsuccessful
     * @param email user's email
     * @param password user's password
     * @param name user's name
     * @return users ID if the transaction is successful
     *
     */
    public long signUpUser(String email, String password, String name){
        long userID = -1;
        createWritableDB();
        createReadableDB();
        userID = getUserIDFromEmailAndPassword(email, password);
        if(userID > 0){
            return userID;
        }

        ContentValues values = new ContentValues();
        values.put(KEY_USER_EMAIL,email);
        values.put(KEY_USER_PSSWD,password);
        values.put(KEY_USER_NAME,name);
        try {
            userID = dbWritable.insertOrThrow(TABLE_USER, null, values);
        } catch(SQLException sqle){
            return userID;
        }
        return userID;
    }


    /*==============================================================================================
    Create tables
    ============================================================================================*/
    public class innerSQLiteOpenHelper extends SQLiteOpenHelper{
        public innerSQLiteOpenHelper(Context context, String name, CursorFactory cursorFactory, int version){
            super(context, name, cursorFactory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db){



            String queryStringCreateItemNameToIdTable=
                    "CREATE TABLE "+ TABLE_ITEM_TO_ITEMID +
                            " ("+
                            "field_item_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE"+
                            ", field_item_name TEXT NOT NULL UNIQUE"+
                            ", field_date_time_added REAL NOT NULL"+
                            ")";

            try{
                db.execSQL(queryStringCreateItemNameToIdTable);
            }catch(SQLException exceptionSQL){
                String message=exceptionSQL.toString();
                String ma=message;
            }


            String queryStringCreateListOfShoppingListsTable=
                    "CREATE TABLE "+ TABLE_SHOPPING_LISTS+
                            " ("+
                            "field_shopping_list_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE"+
                            ", field_shopping_list_name TEXT NOT NULL"+
                            ", field_date_time_added REAL NOT NULL"+
                            ", field_is_shopping_list_checked INTEGER NOT NUll"+
                            ", field_is_shopping_list_deleted INTEGER NOT NULL"+
                            ", UNIQUE(field_shopping_list_name, field_date_time_added)"+
                            ")";

            try{
                db.execSQL(queryStringCreateListOfShoppingListsTable);
            }catch(SQLException exceptionSQL){
                String message=exceptionSQL.toString();
                String ma=message;
            }



            String queryStringCreateListOfItemsInShoppingListTable=
                    "CREATE TABLE "+ TABLE_ITEMS_TO_SHOPPING_LIST+
                            " ("+
                            "field_item_in_shopping_list_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE"+
                            ", field_shopping_list_id INTEGER NOT NULL"+
                            ", field_item_id INTEGER NOT NULL"+
                            ", field_is_item_checked INTEGER NOT NULL"+
                            ", field_is_item_deleted INTEGER NOT NUll"+
                            ", field_items_order_in_the_list INTEGER NOT NULL"+
                            ", field_quantity_for_the_item TEXT NOT NULL"+
                            ", field_date_time_added INTEGER NOT NULL"+
                            ")";
            //", UNIQUE(field_shopping_list_id, field_item_id, field_is_item_deleted)"+ deleted on 2016-03-05

            try{
                db.execSQL(queryStringCreateListOfItemsInShoppingListTable);
            }catch(SQLException exceptionSQL){
                String message=exceptionSQL.toString();
                String ma=message;
            }

            /*
            String queryStringCreateListOfStoresAndIdsTable=
                    "CREATE TABLE TABLE_STORES_AND_IDS"+
                            " ("+
                            "field_store_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE"+
                            ", field_store_name TEXT NOT NULL"+
                            ", field_street_number TEXT NOT NULL"+
                            ", field_street_name TEXT NOT NUll"+
                            ", field_zipcode TEXT NOT NULL"+
                            ", field_state_name TEXT NOT NULL"+
                            ", field_country_name TEXT NOT NULL"+
                            ", UNIQUE(field_store_name, field_street_number, field_street_name, field_zipcode)"+
                            ")";

            try{
                db.execSQL(queryStringCreateListOfStoresAndIdsTable);
            }catch(SQLException exceptionSQL){
                String message=exceptionSQL.toString();
                String ma=message;
            }*/



            String queryStringCreateListOfStoresAndIdsTable=
                    "CREATE TABLE "+ TABLE_STORES_AND_IDS+
                            " ("+
                            "field_store_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE"+
                            ", field_store_name TEXT NOT NUll"+
                            ", field_store_address_string TEXT NOT NULL"+
                            ", field_place_id TEXT NOT NULL UNIQUE"+
                            ")";

            try{
                db.execSQL(queryStringCreateListOfStoresAndIdsTable);
            }catch(SQLException exceptionSQL){
                String message=exceptionSQL.toString();
                String ma=message;
            }



            String queryStringCreateItemtoAisleTable=
                    "CREATE TABLE "+ TABLE_ITEM_TO_AISLE+
                            " ("+
                            "field_entry_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE"+
                            ", field_item_id INTEGER NOT NULL"+
                            ", field_store_id INTEGER NOT NULL"+
                            ", field_aisle_number TEXT NOT NULL"+
                            ")";

            try{
                db.execSQL(queryStringCreateItemtoAisleTable);
            }catch(SQLException exceptionSQL){
                String message=exceptionSQL.toString();
                String ma=message;
            }



            String queryStringCreateDataToTrackItemToAisleAssociationTable=
                    "CREATE TABLE "+ TABLE_ITEM_TO_AISLE_DATA +
                            " ("+
                            "field_entry_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE"+
                            ", field_item_id INTEGER NOT NULL"+
                            ", field_store_id INTEGER NOT NULL"+
                            ", field_aisle_number TEXT NOT NULL"+
                            ", field_check_thumb INTEGER NOT NULL"+
                            ", field_date_time_added REAL NOT NULL"+
                            ")";

            try{
                db.execSQL(queryStringCreateDataToTrackItemToAisleAssociationTable);
            }catch(SQLException exceptionSQL){
                String message=exceptionSQL.toString();
                String ma=message;
            }




            String queryStringCreateItemtoCommentTable=
                    "CREATE TABLE "+ TABLE_ITEM_TO_COMMENT +
                            " ("+
                            "field_entry_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE"+
                            ", field_item_id INTEGER NOT NULL"+
                            ", field_store_id INTEGER NOT NULL"+
                            ", field_item_comment TEXT NOT NULL"+
                            ", field_item_comment_date_time_updated REAL NOT NULL"+
                            ")";

            try{
                db.execSQL(queryStringCreateItemtoCommentTable);
            }catch(SQLException exceptionSQL){
                String message=exceptionSQL.toString();
                String ma=message;
            }

            //--------- EmailID and password table with USERID-------------------
            final String KEY_USER_EMAIL = "userEmail";
            final String KEY_USER_PSSWD = "userPassword";
            final String KEY_USER_NAME = "userName";
            final String KEY_USER_ID = "userID";

            String CREATE_USER_AUTHENTICATION_TABLE = "CREATE TABLE " + TABLE_USER +
                    " (" +
                    KEY_USER_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    KEY_USER_EMAIL + " TEXT NOT NULL," +
                    KEY_USER_PSSWD + " TEXT NOT NULL," +
                    KEY_USER_NAME + " TEXT NOT NULL" +")";

            try {
                db.execSQL(CREATE_USER_AUTHENTICATION_TABLE);
            } catch(SQLException sqle){
                String message = sqle.toString();
                String ma=message;
            }

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
            // doing nothing right now
            if(oldVersion < newVersion) {
                for (String tableName : listOfTables) {
                    db.execSQL("DROP TABLE IF EXISTS " + tableName);
                }
                onCreate(db);
            }
        }

    }

}
