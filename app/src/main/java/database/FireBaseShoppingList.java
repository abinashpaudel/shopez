package database;

import java.util.ArrayList;
import java.util.Date;

import items.Item;

/**
 * Created by abinashpaudel on 4/8/16.
 */
public class FireBaseShoppingList {

    String shoppingListPushID;
    String shoppingListName;
    ArrayList<Item> itemLists;
    long shoppingListID;
    long timeStamp;

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }



    public long getShoppingListID() {
        return shoppingListID;
    }

    public void setShoppingListID(long shoppingListID) {
        this.shoppingListID = shoppingListID;
    }

    public String getShoppingListPushID() {
        return shoppingListPushID;
    }

    public void setShoppingListPushID(String shoppingListPushID) {
        this.shoppingListPushID = shoppingListPushID;
    }

    public String getShoppingListName() {
        return shoppingListName;
    }

    public void setShoppingListName(String shoppingListName) {
        this.shoppingListName = shoppingListName;
    }

    public ArrayList<Item> getItemLists() {
        return itemLists;
    }

    public void setItemLists(ArrayList<Item> itemLists) {
        this.itemLists = itemLists;
    }

    public FireBaseShoppingList(){

    }

    public FireBaseShoppingList(String shoppingListName, ArrayList<Item> itemLists, long shoppingListID){
        this.shoppingListName = shoppingListName;
        this.itemLists = itemLists;
        this.shoppingListID = shoppingListID;
        timeStamp = new Date().getTime();
    }
}
