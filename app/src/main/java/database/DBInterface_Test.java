package database;
//package com.aislefinder.aislefinder;

import android.content.Context;

import com.threecrazyminds.abinash.shopEZ.ShoppingList;

import java.util.ArrayList;
import java.util.Date;

import items.Item;

/**
 * Created by Saaantosh Tripatthi on 3/1/2016.
 */
public class DBInterface_Test {
 DBInterface db_interface;

 public void test_db_interface(Context context){
  long storeId;
  db_interface=DBInterface.getInstance(context);
  long timeValue=new Date().getTime();
       /* storeId=db_interface.addStore("WinCo Foods", "7330", "NE Butler St", "97124", "OR", "US");
        storeId=db_interface.addStore("Costco Wholesale", "1255", "NE 48th Ave", "97124", "OR", "US");
        storeId=db_interface.addStore("Walmart Neighborhood Market", "17275", "NE Cornell Road", "97006", "OR", "US");
        storeId=db_interface.addStore("Walmart Neighborhood Market", "17275", "NE Cornell Road", "97006", "OR", "US");
        storeId=db_interface.getStoreId("Walmart Neighborhood Market", "17275", "NE Cornell Road", "97006", "OR", "US");
        storeId=db_interface.getStoreId("Walmart Neighborhood Market45", "17275", "NE Cornell Road", "97006", "OR", "US");
        */
  long storeId1=db_interface.getStoreId("5000", "A", "B");
  long storeId11=db_interface.getStoreId("5001", "A", "B");
  long storeid2=db_interface.getStoreId("5000", "A", "B");
  long storeId3=db_interface.addStore("5000", "A", "B");






  // items
  long itemId;
  itemId=db_interface.addItem("Banana");
  itemId=db_interface.addItem("Rice");
  itemId=db_interface.addItem("Banana");
  itemId=db_interface.addItem("Milk");
  itemId=db_interface.addItem("Cabbage");
  itemId=db_interface.addItem("Yogurt");
  itemId=db_interface.addItem("Tomato");
  itemId=db_interface.addItem("Tortilla");
  itemId=db_interface.addItem("Tortilla");
  itemId=db_interface.addItem("Potato");
  itemId=db_interface.addItem("Onion");


  // item to aisle
  long aisle1=db_interface.setItemAisle("Banana", 1, "3BANANA");
  long aisle2=db_interface.setItemAisle("Banana", 1, "2BANANA");
  long aisle3=db_interface.setItemAisle("Milk", 1, "2Milk");
  long aisle4=db_interface.setItemAisle("Cabbage", 1, "5Cabbage");
  String aisle5=db_interface.getItemAisle("Cabbage", 1);
  String aisle6=db_interface.getItemAisle("Milk", 1);
  String aisle7=db_interface.getItemAisle("Milk", 2);

  // item to comments
  long comment1=db_interface.setItemComment("Banana", 1, "3BANANA");
  long comment2=db_interface.setItemComment("Banana", 1, "2BANANA");
  long comment3=db_interface.setItemComment("Milk", 1, "2Milk");
  long comment4=db_interface.setItemComment("Cabbage", 1, "5Cabbage");
  String comment5=db_interface.getItemComment("Cabbage", 1);
  String comment6=db_interface.getItemComment("Milk", 1);
  String comment7=db_interface.getItemComment("Milk", 2);



  ArrayList<Item> itemArrayList=new ArrayList<Item>(4);
  itemArrayList.add(new Item("Banana", true, "12"));
  itemArrayList.add(new Item("Milk", false, "14"));
  itemArrayList.add(new Item("Cabbage", false, "14"));
  itemArrayList.add(new Item("Onion", false, "14"));
  ArrayList<String> aisleInfo=db_interface.getAisleInfo(1, itemArrayList);
  aisleInfo=db_interface.getAisleInfo(5000, itemArrayList);
  ArrayList<String> itemNameList=new ArrayList<String>(4);
  itemNameList.add("Banana");
  itemNameList.add("Milk");
  itemNameList.add("Cabbage");
  itemNameList.add("Onion");
  ArrayList<String> thumbStateList=new ArrayList<String>(4);
  thumbStateList.add("UP");
  thumbStateList.add("DOWN");
  thumbStateList.add("NEUTRAL");
  thumbStateList.add("What");
//  db_interface.saveThumbState(1, itemNameList, thumbStateList);




  //shopping list name
  long listId, listIdWinCo;
  double timeVal=1456573112000.0;
  ArrayList<ArrayList<String>> resultList;
  listIdWinCo=db_interface.addShoppingList("BringFromWinco", timeVal);
  listId=db_interface.addShoppingList("BringFromWinco", timeVal);
  listId=db_interface.addShoppingList("BringFromWinco", new Date().getTime());
  listId=db_interface.addShoppingList("BringFromWalmart", new Date().getTime());
  listId=db_interface.addShoppingList("BringFromCostco", new Date().getTime());
  resultList=db_interface.getShoppingLists(1);


  //shopping list items
  ArrayList<Item> itemList=new ArrayList<Item>();
  ArrayList<Item> itemListGet=new ArrayList<Item>();
  itemList.add(new Item("Onion", true, "2kg"));
  itemList.add(new Item("Potato", false, "5kg"));
  itemList.add(new Item("Tortilla", false, "5kg"));
  long shoppingListId=db_interface.saveItems(1, itemList);

  ArrayList<ShoppingList> shoppingLists=db_interface.getShoppingLists();
  long returnVal=db_interface.saveShoppingLists(shoppingLists);




 }


}
