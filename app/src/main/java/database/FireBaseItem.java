package database;

import com.threecrazyminds.abinash.shopEZ.CommonUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by abinash on 3/17/16.
 */
public class FireBaseItem {
    private String itemName;

    private ArrayList<FireBaseItemInfo> itemInfos;

    //sort comparator from small to large
    Comparator<FireBaseItemInfo> compDislikes = new Comparator<FireBaseItemInfo>() {
        @Override
        public int compare(FireBaseItemInfo o1, FireBaseItemInfo o2) {
            if(o1.returnLikePercentage() == o2.returnLikePercentage()){
                return 0;
            }
            if(o1.returnLikePercentage() < o2.returnLikePercentage()){
                return -1;
            }

            return 1;
        }
    };

    //sort comparator from large to small
    Comparator<FireBaseItemInfo> compLikes = new Comparator<FireBaseItemInfo>(){
        @Override
        public int compare(FireBaseItemInfo o1, FireBaseItemInfo o2) {

            int totalRelLikes1 = o1.returnLikePercentage() + o1.returnLikePercentage();
            int totalRelLikes2 = o2.returnLikePercentage() + o2.returnLikePercentage();

            if(totalRelLikes1 == totalRelLikes2){
                return 0;
            }
            if(totalRelLikes1 < totalRelLikes2){
                return 1;
            }

            return -1;
        }
    };


    public FireBaseItem(String itemName) {
        this.itemName = itemName.toLowerCase().trim();
        this.itemInfos = new ArrayList<>();

    }

    public FireBaseItem() {
        this.itemName = null;
        this.itemInfos = null;
    }

    public void setItemInfos(ArrayList<FireBaseItemInfo> itemInfos){
        this.itemInfos = itemInfos;
        if(this.itemInfos!=null){
            Collections.sort(this.itemInfos,compLikes);
        }
    }

    public ArrayList<FireBaseItemInfo> getItemInfos(){
        if(itemInfos!=null)
            Collections.sort(itemInfos,compLikes);
        return itemInfos;
    }


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }


    public void addNewAisleInfo(String aisleNumber, String aisleComment, String section, String subSection, String userID) {
        int myThumbNumber = 1;
        String myAisleNumber = aisleNumber.toLowerCase();


        if(itemInfos == null){
            itemInfos = new ArrayList<FireBaseItemInfo>();
        }

        //remove the item with most dislkes
        if(itemInfos.size()>=3){
            int minDislikesIndex = itemInfos.indexOf(Collections.min(itemInfos,compLikes));
            System.out.println("Removed -->" + itemInfos.get(minDislikesIndex).getAisle() + ", "+itemInfos.get(minDislikesIndex).returnLikePercentage());
            itemInfos.remove(minDislikesIndex);
        }

        //add the new item
        FireBaseItemInfo itemInfo = new FireBaseItemInfo(aisleNumber,
                aisleComment,
                section,
                subSection,
                new Date().getTime()
                ,1,0,userID);
        itemInfos.add(itemInfo);


        //sort the items
        System.out.println("Before Sort -->" + itemInfos.toString());
        Collections.sort(itemInfos, compLikes);
        System.out.println("After Sort -->" + itemInfos.toString());

    }

    public void likeOrDislikeAnAisle(int position, Enum thumbState) {
        int number = 0;
        if(thumbState == CommonUtils.Thumb.UP){
            number =1;
        } else if(thumbState == CommonUtils.Thumb.DOWN){
            number = -1;
        } else{
            return;
        }
        if (number == -1 && itemInfos.get(position).getTotalDislikes() > -100) {
            itemInfos.get(position).setTotalDislikes(itemInfos.get(position).getTotalDislikes() + number);
        } else if (number == 1 && itemInfos.get(position).getTotalLikes() < 100) {
            itemInfos.get(position).setTotalLikes(itemInfos.get(position).getTotalLikes()+number);
        }
    }



}
