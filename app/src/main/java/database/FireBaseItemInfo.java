package database;

/**
 * Created by abinash on 3/26/16.
 */
public class FireBaseItemInfo {


    String aisle;
    String aisleComment;
    long timeStamp;
    int totalLikes;
    int totalDislikes;
    String userID;
    String section;
    String subSection;



    public FireBaseItemInfo(){
        aisle= null;
        aisleComment = null;
        timeStamp = 0;
        totalLikes = 0;
        totalDislikes = 0;
        userID = null;

    }

    public FireBaseItemInfo(String aisle,
                            String aisleComment,
                            String section,
                            String subSection,
                            long timeStamp,
                            int totalLikes,
                            int totalDislikes,
                            String userID){
        this.aisle = aisle;
        this.section = section;
        this.subSection = subSection;
        this.aisleComment = aisleComment;
        this.timeStamp = timeStamp;
        this.totalLikes = totalLikes;
        this.totalDislikes = totalDislikes;
        this.userID = userID;
    }

    public String getAisle() {
        return aisle;
    }

    public void setAisle(String aisle) {
        this.aisle = aisle;
    }

    public String getAisleComment() {
        return aisleComment;
    }

    public void setAisleComment(String aisleComment) {
        this.aisleComment = aisleComment;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(int totalLikes) {
        this.totalLikes = totalLikes;
    }

    public int getTotalDislikes() {
        return totalDislikes;
    }

    public void setTotalDislikes(int totalDislikes) {
        this.totalDislikes = totalDislikes;
    }

    public void setUserID(String userID){
        this.userID = userID;
    }

    public String getUserID(){
        return this.userID;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSubSection() {
        return subSection;
    }

    public void setSubSection(String subSection) {
        this.subSection = subSection;
    }

    public int returnLikePercentage(){
        int totalRelLikes = (getTotalLikes() + getTotalDislikes());
        int totalLikes = (getTotalLikes() - getTotalDislikes());
        return 100 * totalRelLikes/totalLikes;
    }

}
