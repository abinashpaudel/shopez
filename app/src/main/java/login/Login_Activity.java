package login;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.content.Intent;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.threecrazyminds.abinash.shopEZ.CommonUtils;

import database.DBInterface;
import database.FireBaseUtils;
import user.User;
import user.UserUtils;

import com.threecrazyminds.abinash.shopEZ.R;
import com.threecrazyminds.abinash.shopEZ.ShoppingListsMainActivity;

import java.util.Map;


public class Login_Activity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private Activity loginActivity= this;
    private static final int REQUEST_SIGNUP = 0;
    private DBInterface db;
    private boolean createdUser = false;
    private boolean loginSuccess = false;

    User user;


    EditText _emailText;
    EditText _passwordText;
    Button _loginButton;
    Button _skipButton;
    TextView _signupLink;
    TextView _forgotPassword;

    ImageView logo;
    TextView logoText;

    RelativeLayout logoAndText;
    LinearLayout emailAndPasswordText;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        //full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


//        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
//        getActionBar().hide();

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login_);

        //Firebase
        Firebase.setAndroidContext(this);

         _emailText = (EditText) findViewById(R.id.input_email);
         _passwordText = (EditText) findViewById(R.id.input_password);
        _loginButton = (Button) findViewById(R.id.btn_login);
        _skipButton = (Button) findViewById(R.id.btn_skip);
        _signupLink = (TextView) findViewById(R.id.link_signup);
        _forgotPassword = (TextView) findViewById(R.id.forgot_password);

        db = DBInterface.getInstance(this);

        user = CommonUtils.getUser(this);

        if (user != null && user.getUserID().length() > 0) {
            saveUserAndStartShoppingListActivity(user);
            return;
        }

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CommonUtils.hideKeyboard(loginActivity, _passwordText);
                Log.d(TAG, "login");

                if (!validate()) {
                    onLoginFailed();
                    return;
                }

                _loginButton.setEnabled(false);

                final String email = _emailText.getText().toString();
                final String password = _passwordText.getText().toString();

                loginUser("", email, password);
            }
        });

        _skipButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Create default user

                if(_passwordText.getText().length()!=0){
                    new AlertDialog.Builder(loginActivity)
                            .setTitle("Are you Sure?")
                            .setMessage("\nYou entered a password in the textbox but you hit, SKIP LOGIN? \n\n" +
                                    "Was that an accident?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("Skip Login", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    User emptyUser = new User("", "", "");
                                    saveUserAndStartShoppingListActivity(emptyUser);
                                }
                            })
                            .setNegativeButton("Oops, it was an accident!", null).show();
                } else{
                    User emptyUser = new User("", "", "");
                    saveUserAndStartShoppingListActivity(emptyUser);
                }


                return;
            }
        });
        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), Signup_Activity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });

        _forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptForEmailIDAndRequestPasswordReset();
            }
        });


        logo = (ImageView) findViewById(R.id.shopEZIcon);
        logoText = (TextView) findViewById(R.id.logoTV);
        logoAndText = (RelativeLayout) findViewById(R.id.logoRL);
        emailAndPasswordText = (LinearLayout) findViewById(R.id.emailAndPasswordLL);


    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if(hasFocus){
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;

            float logoAndTextFinalPos = -height;
            float skipFinalPos = (height/2.2f);



            Animation leftToRight = AnimationUtils.loadAnimation(loginActivity,R.anim.left_to_right);

            leftToRight.setStartOffset(500);
            leftToRight.setDuration(100);

            _skipButton.startAnimation(leftToRight);

            Animation rightToLeft = AnimationUtils.loadAnimation(loginActivity, R.anim.right_to_left);
            rightToLeft.setStartOffset(1000);
            rightToLeft.setDuration(500);
            emailAndPasswordText.startAnimation(rightToLeft);





        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP && resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            String email = bundle.getString("userEmail");
            String password = bundle.getString("userPassword");
            String userName = bundle.getString("userName");

            //create User
            createAndLoginUser(userName, email, password);

        } else {
            Toast.makeText(this, "SIGNUP Failed... Please try again!!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    private void onNoUserFound() {

        _loginButton.setEnabled(true);

        CommonUtils.shakeAndVibrate(this, _emailText);
        CommonUtils.shakeAndVibrate(this, _passwordText);
        _emailText.setError("invalid email or password");
        _passwordText.setError(null);


    }

    private void onCreateUserFailed(){
        _loginButton.setEnabled(true);
        CommonUtils.shakeAndVibrate(this, _emailText);
        CommonUtils.shakeAndVibrate(this, _passwordText);

    }

    private void saveUserAndStartShoppingListActivity(User user) {
        CommonUtils.storeUser(this, user);
        Intent shoppingListIntent = new Intent(getApplicationContext(), ShoppingListsMainActivity.class);
        startActivity(shoppingListIntent);
        overridePendingTransition(R.anim.zoom_in, R.anim.zoom_exit);
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4) {
            _passwordText.setError("more than 4 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    private void createAndLoginUser(final String userName, final String userEmail, final String password) {

        final ProgressDialog progressDialog = new ProgressDialog(Login_Activity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating User Account ...");
        progressDialog.show();

        Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL);
        ref.createUser(userEmail, password, new Firebase.ValueResultHandler<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> result) {
                try {
                    progressDialog.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }
                loginUser(userName,userEmail,password);
            }

            @Override
            public void onError(FirebaseError firebaseError) {
                try {
                    progressDialog.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }
                UserUtils.handleUserError(firebaseError,loginActivity);
                onCreateUserFailed();
                // there was an error
            }
        });
    }

    private void loginUser(final String userName, final String userEmail, final String password) {

        final ProgressDialog progressDialog = new ProgressDialog(Login_Activity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL);
        ref.authWithPassword(userEmail, password, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                loginSuccess = true;

                try {
                    progressDialog.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }

                //create user
                user = new User(userName, userEmail, authData.getUid());

                //push user to the server
                if(userName.length() > 1) { //If name Length > 0, then the user just signed up
                    pushUserToServer();
                }

                //start Activity
                saveUserAndStartShoppingListActivity(user);
                finish();


            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                progressDialog.dismiss();
                UserUtils.handleUserError(firebaseError,loginActivity);
                onNoUserFound();
            }
        });
    }


    public void pushUserToServer(){

        final Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //add userID if it doesn't exist
                if(dataSnapshot.child("users").child(user.getUserID()).exists() == false) {
                    dataSnapshot.child("users/" + user.getUserID()).getRef().setValue(user);
                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                UserUtils.handleUserError(firebaseError,loginActivity);
            }
        });


    }

    private void promptForEmailIDAndRequestPasswordReset(){

        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(loginActivity);
        final AlertDialog alertDialog;
        LayoutInflater inflater = loginActivity.getLayoutInflater();
        final View alertView = inflater.inflate(R.layout.forgot_password_popup, null);

        final EditText emailIDET = (EditText) alertView.findViewById(R.id.emailID);

        emailIDET.setText(_emailText.getText().toString());

        emailIDET.setSelection(emailIDET.getText().toString().length()); //move cursor to end
        alertBuilder.setView(alertView);


        alertBuilder.setPositiveButton("Request Password Change", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String emailID = emailIDET.getText().toString().trim();
                if (emailID.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(emailID).matches()) {
                    _emailText.setError("enter a valid email address");
                    Toast.makeText(loginActivity,"Enter a valid email ID",Toast.LENGTH_LONG);
                    promptForEmailIDAndRequestPasswordReset();
                } else{
                    requestPasswordReset(emailID);
                }
            }
        });

        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        alertDialog = alertBuilder.show();

        alertDialog.setCanceledOnTouchOutside(true);
    }

    private void requestPasswordReset(String email){
        final Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL);
        final ProgressDialog pd = new ProgressDialog(loginActivity);
        pd.setIndeterminate(true);
        pd.setMessage("Sending Request for Password Change.....");
        pd.show();

        ref.resetPassword(email, new Firebase.ResultHandler() {
            @Override
            public void onSuccess() {
                // password reset email sent
                try {
                    pd.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }
                String myAlertString = "An email has been sent with temporary password."+"\n"
                        + "Check your email.";

                CommonUtils.showSimpleAlertDialog("SUCCESS",myAlertString,loginActivity);
            }

            @Override
            public void onError(FirebaseError firebaseError) {
                try {
                    pd.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }
                UserUtils.handleUserError(firebaseError,loginActivity);
            }
        });
    }

}