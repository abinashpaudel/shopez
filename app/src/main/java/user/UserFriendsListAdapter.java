package user;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.threecrazyminds.abinash.shopEZ.R;

import java.util.ArrayList;

/**
 * Created by abinashpaudel on 4/6/16.
 */
public class UserFriendsListAdapter extends BaseAdapter {

    ArrayList<UserFriend> userFriends;
    Context context;

    public UserFriendsListAdapter(ArrayList<UserFriend> userFriends, Activity context){
        this.userFriends = userFriends;
        this.context = context;
    }

    @Override
    public int getCount() {
        return userFriends.size();
    }

    @Override
    public Object getItem(int position) {
        return userFriends.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class ViewHolder{
        TextView emailTV;
        TextView nameTV;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;
        if(rowView == null){
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.user_friend_row, parent, false);

            holder = new ViewHolder();
            holder.emailTV = (TextView) rowView.findViewById(R.id.userFriendEmail);
            holder.nameTV = (TextView) rowView.findViewById(R.id.userFriendName);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.emailTV.setText(userFriends.get(position).getEmail());
        holder.nameTV.setText(userFriends.get(position).getName());

        return rowView;
    }
}
