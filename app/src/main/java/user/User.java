package user;

/**
 * Created by abinash on 3/27/16.
 */
public class User {
    String userID;
    String userName;
    String userEmail;
    long totalPoints = 0;

    public long getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(long totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public User(){
        userName = null;
        userEmail = null;
        userID = null;
    }

    public User(String userName,String userEmail,String userID){
        this.userName = userName;
        this.userEmail = userEmail;
        this.userID = userID;
    }
}
