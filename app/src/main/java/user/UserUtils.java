package user;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.threecrazyminds.abinash.shopEZ.CommonUtils;

/**
 * Created by abinashpaudel on 4/6/16.
 */
public class UserUtils {

    public UserUtils() {

    }

    public static String decodeEmail(String userEmail) {
        return userEmail.replace(".", ",");
    }

    public static String unCodeEmail(String userEmail) {
        return userEmail.replace(",", ".");
    }

    public static void handleUserError(FirebaseError error, Context context) {
        String errorMsg = "";

        switch (error.getCode()) {
            case FirebaseError.AUTHENTICATION_PROVIDER_DISABLED:
                errorMsg = "The requested authentication provider is disabled for this app. " +
                        "It can be enabled by going into the Login & Auth tab in your app dashboard.";
                break;
            case FirebaseError.EMAIL_TAKEN:
                errorMsg = "The new user account cannot be created because the specified email address is already in use.";
                break;

            case FirebaseError.INVALID_AUTH_ARGUMENTS:
                errorMsg = "The specified credentials are malformed or incomplete." +
                        "Contact threecrazyminds@gmail.com for help";
                break;
            case FirebaseError.INVALID_CONFIGURATION:
                errorMsg = "The requested authentication provider is misconfigured, and the request cannot complete. " +
                        "Please confirm application configuration.";
                break;
            case FirebaseError.INVALID_CREDENTIALS:
                errorMsg = "The specified authentication credentials are invalid. " +
                        "This may occur when credentials are malformed or expired.";
                break;
            case FirebaseError.INVALID_EMAIL:
                errorMsg = "The specified email is not a valid email.";
                break;

            case FirebaseError.INVALID_PASSWORD:
                errorMsg = "The specified user account password is incorrect.";
                break;
            case FirebaseError.INVALID_PROVIDER:
                errorMsg = "The requested authentication provider does not exist. " +
                        "Please consult threecrazyminds@gmail.com for a list of supported providers.";
                break;
            case FirebaseError.NETWORK_ERROR:
                errorMsg = "An error occurred while attempting to contact the authentication server.";
                break;
            case FirebaseError.PREEMPTED:
                errorMsg = "The current request was aborted by a new, incoming authentication request.";
                break;
            case FirebaseError.PROVIDER_ERROR:
                errorMsg = "A third-party provider error occurred. " +
                        "Please refer to the error message and error details for more information.";
                break;
            case FirebaseError.UNKNOWN_ERROR:
                errorMsg = "An unknown error occurred.";
                break;

            case FirebaseError.USER_DOES_NOT_EXIST:
                errorMsg = "The specified user account does not exist.";
                break;

            default:
                errorMsg = "Unknown Error Occured";
                break;


        }

        handleError(errorMsg, context);
    }

    private static void handleError(String errorMsg, Context context) {
        CommonUtils.showSimpleAlertDialog("ERROR!", errorMsg, context);
    }

    public static String getUserLevel(long points) {
        String userLevel = "";

        if (points >= 500) {
            userLevel = "an Expert"+ " (The highest Community Honor)";
        } else if (points >= 400) {
            userLevel = "a Senior"+ " ("+(500-points)+ " to Expert)";
        } else if (points >= 350) {
            userLevel = "Advanced"+ " ("+(400-points)+ " to A Senior)";
        } else if (points >= 300) {
            userLevel = "Experienced"+ " ("+(350-points)+ " to Advanced)";
        } else if (points >= 275) {
            userLevel = "Proficent"+ " ("+(300-points)+ " to Experienced)";
        } else if (points >= 250) {
            userLevel = "Seasoned"+ " ("+(275-points)+ " to Proficient)";
        } else if (points >= 200) {
            userLevel = "Skillful"+ " ("+(250-points)+ " to Seasoned)";
        } else if (points >= 100) {
            userLevel = "Intermediate"+ " ("+(200-points)+ " to Skillful)";
        } else if (points >= 50) {
            userLevel = "Skilled"+ " ("+(100-points)+ " to Intermediate)";
        } else if (points >= 30) {
            userLevel = "Talented"+ " ("+(50-points)+ " to Skilled)";
        } else if (points >= 20) {
            userLevel = "a Beginner"+ " ("+(30-points)+ " to Talented)";
        } else if (points >= 10) {
            userLevel = "a Rookie"+ " ("+(20-points)+ " to Beginner)";
        } else if (points >= 5) {
            userLevel = "a Novice" + " ("+(10-points)+ " to Rookie)";
        } else {
            userLevel = "a Newbie." + " ("+(5-points)+ " to Novice)";
        }
        return userLevel;
    }

    public static int getUserBadges(long points){
        int userBadgeNum = 1;

        if (points >= 500) {
            userBadgeNum = 14;
        } else if (points >= 400) {
            userBadgeNum = 13;
        } else if (points >= 350) {
            userBadgeNum = 12;
        } else if (points >= 300) {
            userBadgeNum = 11;
        } else if (points >= 275) {
            userBadgeNum = 10;
        } else if (points >= 250) {
            userBadgeNum = 9;
        } else if (points >= 200) {
            userBadgeNum = 8;
        } else if (points >= 100) {
            userBadgeNum = 7;
        } else if (points >= 50) {
            userBadgeNum = 6;
        } else if (points >= 30) {
            userBadgeNum = 5;
        } else if (points >= 20) {
            userBadgeNum = 4;
        } else if (points >= 10) {
            userBadgeNum = 3;
        } else if (points >= 5) {
            userBadgeNum = 2;
        } else {
            userBadgeNum = 1;
        }

        return userBadgeNum;
    }

    public static void sendEmail(Context context, String emailToSend, String subject,
                                 String body) {


        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{emailToSend});
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT, body);

        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);

        try {
            InputMethodManager imm = (InputMethodManager)context.getSystemService(Service.INPUT_METHOD_SERVICE);

            context.startActivity(Intent.createChooser(i, "Send mail..."));

            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
