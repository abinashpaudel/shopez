package user;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.threecrazyminds.abinash.shopEZ.CommonUtils;
import com.threecrazyminds.abinash.shopEZ.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import database.DBInterface;
import database.FireBaseShoppingList;
import database.FireBaseUtils;
import items.Item;
import items.ItemListsActivity;

public class ShareListActivity extends AppCompatActivity {

    private ArrayList<UserFriend> userFriends;
    private UserFriendsListAdapter userFriendsListAdapter;

    private Activity context = this;
    User user;

    ShowcaseView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();

        if (ab != null) {
            ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
            ab.setDisplayShowTitleEnabled(true);
        }

        TextView titleTV = (TextView) findViewById(R.id.toolbar_title);
        titleTV.setText("Share List");

        user = CommonUtils.getUser(context);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sv!=null && sv.isShowing()){
                    sv.hide();
                }

                Intent addFriendActivity = new Intent(getApplicationContext(), AddFriendActivity.class);
                startActivity(addFriendActivity);
//                context.finish();
                overridePendingTransition(R.transition.left_in, R.transition.right_out);
            }
        });



        setUserFriends();

        userFriendsListAdapter = new UserFriendsListAdapter(userFriends, this);

        ListView userFriendLV = (ListView) findViewById(R.id.friendsListLV);

        userFriendLV.addFooterView(CommonUtils.getEmptyFooterView(context));

        userFriendLV.setAdapter(userFriendsListAdapter);

        userFriendLV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(context)
                        .setTitle("Confirm")
                        .setMessage("Remove " + userFriends.get(position).getName() + " from friend list?")
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {


                            public void onClick(DialogInterface dialog, int whichButton) {

                                removeFriend(userFriends.get(position).getEmail(),
                                        userFriends.get(position).getName());

                                userFriends.remove(position);
                                userFriendsListAdapter.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                return true;
            }

        });

        userFriendLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                String shoppingListName = CommonUtils.getShopListName(context);
                new AlertDialog.Builder(context)
                        .setTitle("Confirm")
                        .setMessage("Share " + shoppingListName + " with " + userFriends.get(position).getName() + "?")
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {


                            public void onClick(DialogInterface dialog, int whichButton) {
                                pushShoppingListToFireBase(position);

                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });

        showFirstTimeTutorial();

    }

    private void showFirstTimeTutorial(){

        if((CommonUtils.activityStartedFirst(CommonUtils.SHARE_LIST_ACTIVITY,context) == false
                || userFriends.size()>0)
                ){
            return;
        }

        sv = new ShowcaseView.Builder(context)
                .setTarget(new ViewTarget(R.id.fab,this))
                .setContentTitle("Add Friends to share the list with your friends")
                .setContentText("")
                .hideOnTouchOutside()
                .build();

        sv.hideButton();

        sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
            @Override
            public void onShowcaseViewHide(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                CommonUtils.setFirstTimeActivityToFalse(CommonUtils.SHARE_LIST_ACTIVITY,context);
            }

            @Override
            public void onShowcaseViewShow(ShowcaseView showcaseView) {

            }

            @Override
            public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

            }
        });



    }

    private void removeFriend(final String email, final String name){

        if(user.getUserID().length()<4){
            CommonUtils.showCustomAlertDialog("NOT SIGNED IN", "You must be signed in to delete friends from cloud", context);
            return;
        }

        final String decodedUserEmail = UserUtils.decodeEmail(user.getUserEmail());
        final String friendEmail = UserUtils.decodeEmail(email);

        final Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL+"/userFriends/"+decodedUserEmail+"/"
        +friendEmail);

        ref.removeValue(new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                System.out.println("Remove Successfully");
                Toast.makeText(context, "Friend " + name + " remove successfully", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void pushShoppingListToFireBase(final int position){
        if(user.getUserEmail().length()<0){
            CommonUtils.showCustomAlertDialog("NOT SIGNED IN", "You must be signed in to share the shopping list", context); ;
            return;
        }

        if(CommonUtils.isNetworkAvailable(context) == false){
            CommonUtils.showCustomAlertDialog("NO CONNECTION", "Hmnn... Seems like you don't have internet connection.." +
                    "\n"+"Enable the connection and try again!",context);
            return;
        }

        final ProgressDialog pd = new ProgressDialog(context);
        pd.setIndeterminate(true);
        pd.setMessage("Creating a shareable List....");
        pd.show();


        removeOldShoppingListFromFireBase();

        final String shoppingListName = CommonUtils.getShopListName(context);
        final long shoppingListID = CommonUtils.getShoppingListID(context);
        final String decodedUserEmail = UserUtils.decodeEmail(user.getUserEmail());

        ArrayList<Item> shoppingListItems = DBInterface.getInstance(context).getItemList(shoppingListID);

        final FireBaseShoppingList sharedList = new FireBaseShoppingList(shoppingListName,shoppingListItems,shoppingListID);

        Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL);


        //push
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean foundOldShoppingList = false;
                String shoppingListPushID = "";
                DataSnapshot shoppingListKeyPath = dataSnapshot.child("userSharedList").child(decodedUserEmail);
                if (shoppingListKeyPath.exists()) {
                    for (DataSnapshot shoppingListKey : shoppingListKeyPath.getChildren()) {
                        if (shoppingListKey.child("shoppingListID").getValue(Long.class) == shoppingListID) {
                            //old shopping List is found
                            shoppingListKey.getRef().setValue(sharedList);
                            foundOldShoppingList = true;
                            shoppingListPushID = shoppingListKey.getKey().toString();
                            System.out.println("Old shopping list was found..");
                            break;
                        }
                    }
                }

                //if old Item is not found, then push
                if (foundOldShoppingList == false) {
                    Firebase refTempNewShoppingList = dataSnapshot.child("userSharedList").child(decodedUserEmail).getRef().push();
                    shoppingListPushID = refTempNewShoppingList.getKey().toString();
                    sharedList.setShoppingListPushID(shoppingListPushID);
                    refTempNewShoppingList.setValue(sharedList);
                    System.out.println("No shopping list was found");

                }


                String decodedUserFriendEmail = UserUtils.decodeEmail(userFriends.get(position).getEmail());
                String userFriendName = userFriends.get(position).getName();

                UserFriend userFriend = new UserFriend(userFriendName, decodedUserFriendEmail);

                Map<String, Object> ownerInfo = new HashMap<String, Object>();
                ownerInfo.put("ownerEmail", decodedUserEmail);//key and value are same

                DataSnapshot sharedWithPath = dataSnapshot.child("sharedWith").child(decodedUserFriendEmail).child(shoppingListPushID);
                sharedWithPath.getRef().setValue(ownerInfo);
                System.out.println("Set new value");

                try {
                    pd.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }

                new AlertDialog.Builder(context)
                        .setTitle("SUCCESS")
                        .setMessage("SWEET! The list \"" + shoppingListName + "\" has now been shared with \"" + userFriendName + "\".\n\n" +
                                "Would you like to let him know via email?")

                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setPositiveButton("Yes, Email", new DialogInterface.OnClickListener() {


                            public void onClick(DialogInterface dialog, int whichButton) {
                                sendEmailNotification(position);

                            }
                        })
                        .setNegativeButton("Skip Email", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent itemListActivity = new Intent(getApplicationContext(), ItemListsActivity.class);
                                startActivity(itemListActivity);
                                context.finish();
                                overridePendingTransition(R.transition.right_in, R.transition.left_out);
                            }
                        })

                        .show();

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                try {
                    pd.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }
                CommonUtils.showCustomAlertDialog("ERROR", "Couldn't create a shareable list", context);
            }
        });
    }

    private void removeOldShoppingListFromFireBase(){
        Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL);
        final String decodedUserEmail = UserUtils.decodeEmail(user.getUserEmail());
        final long nowTime = new Date().getTime();

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DataSnapshot shoppingListKeyPath = dataSnapshot.child("userSharedList").child(decodedUserEmail);
                if (shoppingListKeyPath.exists()) {
                    for (DataSnapshot shoppingListKey : shoppingListKeyPath.getChildren()) {
                        long shoppingListPushTime = shoppingListKey.child("timeStamp").getValue(Long.class);
                        int daysSince = (int)((nowTime - shoppingListPushTime) / (1000*60*60*24l));
                        if (Math.abs(daysSince) >=10 ) { //If older than 10 days.....
                            shoppingListKey.getRef().removeValue();

                        }
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private void sendEmailNotification(int position){

        String email = userFriends.get(position).getEmail();
        String name = userFriends.get(position).getName();
        String subject = "shopEZ List Shared";
        String body="Hi "+name + ",\n\n" +
                "I have shared a shopping list with you using shopEZ. \n" +
                "Login to shopEZ to accept my shopping list.\n\n\n" +
                "Psst.... download the app from here \"https://play.google.com/apps/testing/com.threecrazyminds.abinash.shopEZ\" if you don't have it installed!";
        UserUtils.sendEmail(context,email,subject,body);
    }

    private void setUserFriends() {

        if (userFriends == null) {
            userFriends = new ArrayList<UserFriend>();
        }

        if (user.getUserID().length() < 4) {
            return;
        } else {

            fetchFriends(user);

        }
    }

    private void fetchFriends(User user) {

        if(CommonUtils.isNetworkAvailable(context) == false){
            CommonUtils.showCustomAlertDialog("NO CONNECTION", "Hmnn... Seems like you don't have internet connection.." +
                    "\n"+"Enable the connection and try again!",context);
            return;
        }
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Getting Friend List");
        pd.setIndeterminate(true);
        pd.show();

        String userEmail = UserUtils.decodeEmail(user.getUserEmail());
        System.out.println(userEmail);
        Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL + "/userFriends/" + userEmail);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int friendsCount = (int) dataSnapshot.getChildrenCount();
                if (friendsCount == 0) {
                    try {
                        pd.dismiss();
                    } catch(Exception e){
                        System.out.println("Dialog dismissed after activity finished....");
                    }
                    Toast.makeText(context, "No friends found..", Toast.LENGTH_SHORT).show();
                }
                for (DataSnapshot fbUserFriends : dataSnapshot.getChildren()) {
                    UserFriend userFriend = fbUserFriends.getValue(UserFriend.class);
                    userFriends.add(userFriend);
                }
                if (userFriendsListAdapter != null) {
                    userFriendsListAdapter.notifyDataSetChanged();
                }

                try {
                    pd.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                try {
                    pd.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }
                Toast.makeText(context, "Cancelled!", Toast.LENGTH_SHORT).show();

            }
        });
    }



}
