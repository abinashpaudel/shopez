package user;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.threecrazyminds.abinash.shopEZ.CommonUtils;
import com.threecrazyminds.abinash.shopEZ.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import database.FireBaseUtils;

public class AddFriendActivity extends AppCompatActivity {

    private ArrayList<String> users;
    private ArrayList<String> names;

    private Activity context= this;
    ArrayAdapter activeUsersListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView titleTV = (TextView) findViewById(R.id.toolbar_title);
        titleTV.setText("Add Friend");

        final ActionBar ab = getSupportActionBar();

        if (ab != null) {
            ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
            ab.setDisplayShowTitleEnabled(true);
        }

        ListView addFriendLV = (ListView) findViewById(R.id.userListLV);
        addFriendLV.addFooterView(CommonUtils.getEmptyFooterView(this));

        users = new ArrayList<>();
        names = new ArrayList<>();

        final User currUser = CommonUtils.getUser(this);
        getAllActiveUsers(currUser);

        activeUsersListAdapter = new ArrayAdapter(this,  android.R.layout.simple_list_item_1, users);

        addFriendLV.setAdapter(activeUsersListAdapter);

        addFriendLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                addUserAsFriend(position,currUser);
            }
        });


    }

    private void addUserAsFriend(final int position, final User currUser){
        new AlertDialog.Builder(this)
                .setTitle("Confirm")
                .setMessage("Add "+users.get(position)+" as a friend?")
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        pushFriend(users.get(position), names.get(position), currUser);

                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void pushFriend(final String email, final String name, final User currUser){


        if(currUser.getUserID().length()<4){
            CommonUtils.showCustomAlertDialog("NOT SIGNED IN", "You must be signed in to add friends",context);
            return;
        }

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Add Friend to your List...");
        pd.setIndeterminate(true);
        pd.show();

        final String decodedUserEmail = UserUtils.decodeEmail(currUser.getUserEmail());
        final String friendEmail = UserUtils.decodeEmail(email);

        final Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL+"/userFriends");



        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                UserFriend userFriend = new UserFriend(name,friendEmail);


                Map<String,Object> friendEmailAndName = new HashMap<String,Object>();
                friendEmailAndName.put(friendEmail,userFriend);


                if(dataSnapshot.child(decodedUserEmail).exists()){
                    dataSnapshot.child(decodedUserEmail+"/"+friendEmail).getRef().setValue(userFriend);
                } else{
                    System.out.println(dataSnapshot.child("/" + decodedUserEmail).getRef().toString());
                    dataSnapshot.child(decodedUserEmail).getRef().setValue(friendEmailAndName);
                }

                pd.dismiss();
                switchToShareListActivity();
                Toast.makeText(context, "Added "+name, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

                pd.dismiss();
                switchToShareListActivity();
                Toast.makeText(context, "Couldn't add friend", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void switchToShareListActivity(){
        Intent shareListActivity = new Intent(getApplicationContext(),ShareListActivity.class);
        startActivity(shareListActivity);
        context.finish();
    }

    private void getAllActiveUsers(final User user){

        if(user.getUserID().length()<4){
            CommonUtils.showCustomAlertDialog("NOT SIGNED IN", "You must be signed in to see your friends..",context);
            return;
        }

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Getting Users.....");
        pd.setIndeterminate(true);
        pd.show();

        String userEmail = UserUtils.decodeEmail(user.getUserEmail());

        final Firebase ref = new Firebase(FireBaseUtils.FIREBASE_URL+"/users");
        Query queryRef = ref.orderByChild("userEmail");

        queryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot fbUsers : dataSnapshot.getChildren()){

                    String fbUserEmail = dataSnapshot.child(fbUsers.getKey().toString()+"/userEmail").getValue(String.class);
                    String fbUserName = dataSnapshot.child(fbUsers.getKey().toString()+"/userName").getValue(String.class);

                    if(!fbUserEmail.equals(user.getUserEmail())) {
                        users.add(fbUserEmail);
                        names.add(fbUserName);
                    }
                }
                if(activeUsersListAdapter!=null){
                    activeUsersListAdapter.notifyDataSetChanged();
                }

                try {
                    pd.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                try {
                    pd.dismiss();
                } catch(Exception e){
                    System.out.println("Dialog dismissed after activity finished....");
                }
                Toast.makeText(context,"Cancelled!",Toast.LENGTH_SHORT).show();
            }
        });
    }

}
