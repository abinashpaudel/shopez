package user;

/**
 * Created by abinashpaudel on 4/6/16.
 */
public class UserFriend {

    private String name;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {

        this.email = UserUtils.unCodeEmail(email);
    }

    public UserFriend(String name, String email){
        this.name = name;
        this.email = email;
    }

    public UserFriend(){
        name = null;
        email = null;
    }
}
